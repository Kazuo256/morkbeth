sitegen = require 'sitegen'
Site = require "sitegen.site"
tables = require 'db.tables'
table_formatter = require 'format.table'
discount = require 'discount'

site = Site!

site.config.out_dir = "public/"

content_counter = 1
content_format = [[
<div class="w3-card-4">
  <button onclick="toggleSection('content-%d')" class="w3-button w3-block w3-left-align">
    <h3>%s <br class="w3-hide-large"/>%s</h3>
  </button>
	<div class="w3-container w3-hide" id="content-%d">
		%s
	</div>
</div>
]]

tag_format = [[
<span class="w3-tag w3-padding-small w3-round-large w3-dark-grey w3-right" style="margin-right: 0.5em; margin-bottom: 8px">%s</span>]]

load_site = =>
  @title = "Diário dos Exilados de Morkbeth"
  @root = "/morkbeth"
  @page_rules_combat = "rules/04_combat"
  @character_pages = {
    { file: "rules/02a_character_intro", title: "Novos Personagens"},
    { file: "rules/02b_character_scores", title: "Pontos de Habilidade"},
    { file: "rules/02c_character_options", title: "Opções na Criação"},
    { file: "rules/02d_character_gods", title: "Deuses e Divindades"},
    { file: "rules/02e_character_progression", title: "Progressão"},
  }
  @rule_pages = {
    { file: "rules/01_intro", title: "1. Introdução"},
    { file: "rules/03_adventures", title: "3. Aventuras"}
    { file: @page_rules_combat, title: "4. Combate"}
    { file: "rules/05_equipment", title: "5. Equipamento"}
    { file: "rules/06_downtime", title: "6. Tempo Livre"}
    { file: "rules/07_crafting", title: "7. Manufatura"}
  }
  @info_pages = {
    { file: "info/expansions", title: "Expansões, pesquisas e conquistas" }
    { file: "info/equipment", title: "Equipamento" }
  }
  @asi_by_background_1 = tables.asi_by_background_1
  @asi_by_background_2 = tables.asi_by_background_2
  @xp_award_per_region_level = tables.xp_award_per_region_level
  @bonus_and_penalty_per_travel_mode = tables.bonus_and_penalty_per_travel_mode
  @light_levels = tables.light_levels
  @sources_of_light = tables.sources_of_light
  @magic_sources = tables.magic_sources
  @room_sizes = tables.room_sizes
  @lifestyles = tables.lifestyles

  @beginrow = [[<div class="w3-row">]]
  @endrow = [[</div>]]
  @begincolumn = [[<div class="w3-container w3-half">]]
  @endcolumn = [[</div>]]

  @begindoublecolumn = [[<div class="w3-container">]]
  @enddoublecolumn = [[</div>]]

  @begincard = [[<div class="w3-card-4"><div class="w3-container">]]
  @endcard = [[</div></div>]]

  @vspace = [[<span style="display: block; height: 2em"></span>]]

  @data = require "db.data"

  @table = (arg) => table_formatter(arg[1])

  @content = (arg) =>
    tags = {}
    for _, tag in ipairs arg[1].tags
      table.insert tags, (string.format tag_format, tag)
    tags = table.concat tags, ""
    id = content_counter
    content_counter += 1
    rules = assert discount arg[1].rules
    rules = string.gsub rules, "table:([%w_]+)", table_formatter
    string.format content_format, id, arg[1].name, tags, id, rules

  (require "db.keywords") @

  add "index.md"

  for _, page in ipairs @character_pages
    add page.file .. ".md"

  for _, page in ipairs @rule_pages
    add page.file .. ".md"

  for _, page in ipairs @info_pages
    add page.file .. ".md"


sitegen.create load_site, site
