
# Expansões e Conquistas

<div class="w3-row">
  <div class="w3-container w3-col" style="width:16.67%"></div>
  $table{ "expansion_points" }
  <div class="w3-container w3-col" style="width:16.67%"></div>
</div>

<div class="w3-row" style="margin-top: 1em">
  <div class="w3-container w3-quarter"></div>
  $table{ "expansion_points_distribution" }
  <div class="w3-container w3-quarter"></div>
</div>


