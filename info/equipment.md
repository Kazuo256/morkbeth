# Equipamento

## Armas mundanas

$beginrow
$begindoublecolumn

$table{ "simple_melee_weapon_crafting" }

$enddoublecolumn
$endrow

$beginrow
$begindoublecolumn

$table{ "martial_melee_weapon_crafting" }

$enddoublecolumn
$endrow

## Itens mágicos

$beginrow
$begincolumn

Alguns itens novos que complementam o conteúdo de Morkbeth. Parte deles tem sua
fórmula disponível na tabela *Fórmulas para Itens Mágicos Diversos*.

$content{ $data|potion_of_power }

$content{ $data|crystal_of_return }

$content{ $data|cipher }

$content{ $data|talisman_of_burden }

$content{ $data|kingsblade }

$content{ $data|weapon_of_shadowbleeding }

$endcolumn
$endrow

