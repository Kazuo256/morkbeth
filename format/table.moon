
tables = require 'db.tables'

table_format = [[
<div class="w3-container %s">
  %s
  <table class="w3-table-all w3-card-4">
    <tr class="w3-dark-gray">
      %s
    </tr>
    %s
  </table>
</div>
]]

header_format = [[
    <th class="%s">%s</th>
]]

row_format = [[
  <tr>
    %s
  </tr>
]]

cell_format = "<td class='%s'>%s</td>\n"

(id) ->
  if tab = tables[id]
    headers = ""
    for i,header in ipairs(tab.headers)
      column_class = tab.column_class and tab.column_class[i] or ""
      headers ..= string.format header_format, column_class, header
    rows = ""
    for row in *tab.rows
      cells = ""
      for i, cell in ipairs(row)
        column_class = tab.column_class and tab.column_class[i] or ""
        cells ..= string.format cell_format, column_class, cell
      rows ..= string.format row_format, cells
    title = ""
    if tab.title
      title = string.format "<h4>%s</h4>", tab.title
    string.format table_format, tab.table_class or "", title, headers, rows
  else
    string.format "<i>Table not found: %s</i>", id

