
# Manufatura

$begincolumn

Manufatura é um processo pelo qual você (ou um NPC contratado) usa uma fórmula
para transformar materiais em um item ao longo de uma ou mais semanas.

Um material tem tipo, raridade e (às vezes) um modificador. Além disso, cada
tipo de material tem propriedades diferentes, que determinam quais fórmulas são
compatíveis.

Os tipos de material e suas propriedades, assim como seus pesos e valores base
(na raridade comum), estão descritos na tabela *Tipos de Material*. Por
exemplo, você pode encontrar um *Ingot of Common Metal* (tipo metal, raridade
comum, sem modificador) ou uma *Shard of Rare, Incandescent Gem* (tipo gema,
raridade rara, modificador *incandescent*).

Materiais de raridade superior ou com modificadores valem mais. Em geral,
materiais são aceitos como moeda de troca sem perda nenhuma de valor mas só
podem ser comprados se estiverem acessíveis a Sharpridge. Além
disso, é possível quebrar um material em cópias de raridade menor. Os valores
e proporções dessas operações estão listados na tabela *Conversão de Valores de
Materiais*.

Quando você manufatura um item por conta própria através de uma atividade de
tempo livre, o mínimo de tempo gasto é uma semana mas, dependendo da raridade
dos materiais usados, esse tempo aumenta conforme a tabela de *Tempo Adicional
por Raridade de Materiais*. O tempo adicional é acrescido para cada material
usado da raridade correspondente. Você pode trabalhar em equipe para acelerar o
processo ou dividir o progresso da manufatura entre aventuras.  Se o item for
consumível, você pode fazer até quatro deles no tempo de um.

$endcolumn
$begincolumn

$vspace
$vspace

<div class="w3-row" style="margin-top: 1em">
  $table{ "material_value" }
  <div class="w3-container w3-rest"></div>
</div>

$vspace

<div class="w3-row" style="margin-top: 1em">
  $table{ "crafting_time" }
  <div class="w3-container w3-rest"></div>
</div>

$vspace

$endcolumn

$begindoublecolumn

<div class="w3-row" style="margin-top: 1em">
  <div class="w3-container w3-col" style="width: 16.67%"></div>
  $table{ "material_types" }
  <div class="w3-container w3-rest"></div>
</div>

$enddoublecolumn

## Propriedades dos Materiais

$beginrow
$begincolumn

Existem 6 propriedades que tipos de materiais podem ter. Cada propriedade
indica em que tipo de equipamento o material pode ser usado. Por exemplo,
materiais com *harm* são usado em armas e munições. Algumas propriedades
possuem *parâmetros* que limitam ainda mais como o material é usado.

Para que o efeito de uma propriedade seja aplicado em um item manufaturado, o
material precisa ter sido usado na fórmula *satisfazendo aquela propriedade*.
Quando uma fórmula exige materiais de certo tipo e *todos* os mateirias
fornecidos daquele tipo tem raridade pelo menos um nível acima que o
especificado, o item resultante se torna mágico e o efeito das propriedades dos
materiais podem ser ampliado.  Para cada nível de raridade que *todos* os
materiais de um tipo exigido possuem, o efeito do modificador aumenta em um
passo. Quando for o caso, haverá uma seção ***raridades maiores*** na descrição
da propriedade explicando exatamente como o efeito aumenta.

$endcolumn
$begincolumn

$content{ $data|structure }

$content{ $data|harm }

$content{ $data|protection }

$content{ $data|consumable }

$content{ $data|valuable }

$endcolumn
$endrow

## Fórmulas

$beginrow
$begincolumn

Aqui ficam as fórmulas para fazer os equipamentos mundanos encontrados no PHB
(e mais alguns novos). Mesmo que um item (mundano ou mágico) não apareça nas
listas aqui, não quer dizer que não seja possível manufaturá-lo. Converse com o
DM a respeito.

As tabelas *Fórmulas para XXX* agrupam equipamentos por "famílias", cada uma
com uma fórmula geral que pode ser especializada para produzir equipamentos
específicos. Cada família especifica as propriedades base de seus equipamentos
e cada fórmula específica mostra como o equipamento se diferencia dos outros em
sua família. Por exemplo, uma *flail* é um tipo de *mace* que causa mais dano e
é uma arma marcial. Para saber quais materiais sua manufatura exige, basta
somar a fórmula geral para *maces* à fórmula adicional de *flails*, ambas na
tabela Fórmulas para *Maces*.

$endcolumn
$begincolumn

No caso, a fórmula requer 2 materiais com propriedade *Structure (hard)* e um
material com propriedade *Harm (advanced)*. Você pode usar 2 toras de Common
Wood e uma barra de Common Metal, ou 2 pilhas de Common Bone e 1 minério de
Common Mineral.

Algumas armas muito simples não requerem manufatura, como *clubs* e *slings*.
Outras podem exigir materiais com um mínimo de raridade, algo comum entre armas
à distância e em várias armaduras. Além do mais, várias fórmulas produzem mais
de uma unidade do equipamento indicado.  Preste atenção para ver se seu preço
indicado é unitário ou total!

Por fim, esse sistema de manufatura é relativamente complexo, então qualquer
dúvida não hesite em procurar o DM. Também peço paciência caso haja muitas
erratas e re-balanceamentos, mas agradeço muito toda ajuda!

$endcolumn
$endrow

## Modificadores

$beginrow
$begincolumn

Como dito antes, materiais podem ter diversos modificadores. Aqui listamos todos
os modificadores e o efeito básico de cada um. Os efeitos básico de um
modificador são aplicados a qualquer equipamento manufaturado usando pelo menos
um material com aquele modificador, contanto que o material não tenha sido
expressamente requerido pela fórmula.

$endcolumn
$begincolumn

$content{ $data|fine_mod }

$content{ $data|dense_mod }

$content{ $data|sharp_mod }

$content{ $data|incandescent_mod }

$content{ $data|freezing_mod }

$content{ $data|corrosive_mod }

$content{ $data|charged_mod }

$content{ $data|toxic_mod }

$content{ $data|profane_mod }

$content{ $data|holy_mod }

$content{ $data|warped_mod }

$content{ $data|resonant_mod }

$content{ $data|occult_mod }

$endcolumn
$endrow

## Materiais únicos

$beginrow
$begincolumn

Algums materiais (principalmente de raridade
incomum ou maior) possuem um nome único. Eles correspondem a um material
comum da mesma maneira que alguns equipamentos mágicos são baseados em
equipamentos mundanos. Materiais únicos conferem um ou mais efeitos especiais
a itens manufaturados com eles, a não ser que sua fórmula exija especificamente
aquele material único. Por exemplo, um pedaço de *casco de Tartaruga de Chifre*
tem como base uma pilha de *ossos afiados e incomuns*.

$endcolumn
$endrow
