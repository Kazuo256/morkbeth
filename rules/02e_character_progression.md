
# Progressão

Nesta campanha, cada personagem tem sua própria progressão individual, mas
todos que participam de uma mesma sessão ganham a mesma quantidade de XP (salvo
alguma situação extraordinária que justifique o contrário). **Todo personagem
novo começa sempre no nível 1**. Não usamos CR para determinar o XP ganho. Ao
invés disso, usamos o seguinte método ([inspirado neste
artigo](https://theangrygm.com/angrys-guide-to-experience/)).

Em Morkbeth, toda região tem um nível associado. A dificuldade dos desafios de
uma aventura depende apenas dos níveis das regiões onde ela se passa. Isso
inclui tanto encontros de combate quanto outros obstáculos que os jogadores
precisem superar. Os heróis ganham XP na forma de **prêmios** quando superam
desafios e quando cumprem objetivos. Pode ser um objetivo da aventura ou um
objetivo individual de um personagem — em todo caso, todos ganham o prêmio. A
quantidade de XP depende do tamanho e tipo do prêmio. A tabela de *XP por
Prêmio e Nível da Região* (sujeita a futuras alterações) mostra quanto de XP
exatamente um personagem ganha por prêmio em uma dada região. Fora isso,
**personagens só avançam de nível durante *downtime* (entre uma aventura e
outra)**.

Assim, superar um obstáculo razoável numa Região de Nível 3 SEMPRE vale 150 XP
para cada jogador e ponto. Não precisa dividir nem nada. Obstáculos menores
podem valer Meio Prêmio e encontros climáticos valem um Duplo Prêmio. Encontros
que acontecem devido a complicações ou simplesmente aleatórios são apenas
distrações e não valem nenhum Prêmio.

#### XP por Prêmio e Nível da Região

<table class="w3-table-all w3-card-4 w3-centered">
  <tr class="w3-dark-gray">
    <th>Nível da Região</th>
    <th>Meio Prêmio</th>
    <th>Prêmio Completo</th>
    <th>Prêmio Duplo</th>
  </tr>
  $xp_award_per_region_level[[<tr><td>$lv</td><td>$half</td><td>$full</td><td>$double</td></tr>]]
</table>

<span style="height:2em"></span>

## Pontos de Vida

Ao passar de nível em uma classe, os personagens sempre ganham a quantidade
média de pontos de vida indicada na descrição da tal classe.

