
# Introdução

$begincolumn
$begincard

#### Resumão
+ Você foi exilado para terras selvagens
+ Você começa com apenas um item
+ Aventuras = expedições independentes
+ Jogadores variam entre sessões
+ Quanto mais longe, mais perigoso
+ Jogadores decidem onde ir / o que fazer
+ Nada de mapas e mordomias
+ O perigo é constante!

$endcard
$endcolumn

$begincolumn

Esta é uma campanha no estilo *West Marches*. Nela, os jogadores se aventuram
para além dos limites da civilização, onde ninguém mais se atreve ir. Uma
fronteira com terras selvagens dominadas por bestas e outras criaturas
aterrorizantes.  Explorar esses lugares é tão arriscado que os heróis da
campanha não conseguem passar muito tempo nelas.  Eles precisam realizar
incontáveis expedições e planejar cada avanço com cautela para garantir que
voltarão para contar a história.

$vspace
$endcolumn


## O que é *West Marches*?

É um tipo de campanha de RPG bastante diferente, com forte características de
*sandbox*. Segundo o criador do gênero, as principais regras são:

***Sem sessões regulares***. Cada sessão é marcada pelos jogadores por demanda.

***Sem grupo regular***. Cada sessão tem jogadores diferentes. No total, pode
haver 10, 20, 30 jogadores, ou mais. Também podem haver múltiplos mestres.

***Sem história predeterminada***. Os jogadores decidem onde ir e o que fazer.
Quando uma sessão é agendada, os jogadores dizem ao mestre o que pretendem
fazer para que o mestre possa se preparar.

Para que essas regras façam sentido, o cenário de uma campanha West Marches
costuma ter uma **Base** e um **Território Selvagem**. Toda sessão, os
jogadores partem da Base para algum lugar por eles escolhidos no Território
Selvagem. Eles exploram, caçam, descobrem, destroem, pilham, resgatam ou fazem
o que bem planejaram neste lugar e depois voltam para a Base.

Assim, toda aventura é uma história auto-contida, similar a um one-shot, mas –
como o mundo é sempre o mesmo – existe progressão e consequências duradouras
para as ações dos jogadores. Como consequência dos participantes de cada sessão
serem diferentes, parte do espírito de uma campanha West Marches é incentivar
os jogadores a colaborarem entre si. Isso, em geral envolve:

***Compartilhar experiências***. Para lembrar de pontos de interesse, seja
para explorar novamente ou nunca mais pisar dentro, é importante que os
jogadores contem uns pros outros o que eles fizeram e descobriram durante suas
aventuras. Parte por parte, eles vão construindo a história do mundo.

***Compartilhar mapas***. O Território Selvagem é desconhecido, portanto
não existem mapas dele. Para trazer essa experiência aos jogadores, os mestres
nunca revelam para eles nenhum mapa em escala do Território Selvagem como um
todo. É responsabilidade dos jogadores rabiscar seus próprios esquemas de como
eles entendem que o Território é composto, mesmo que eles anotem errado.

<img src='https://www.gmbinder.com/assets/img/books.jpg' style='mix-blend-mode:multiply; width:50%' />

## Dificuldade e perigo

Como dá para ver, uma campanha West Marches exige bastante independência e
autonomia dos jogadores como parte da liberdade que eles ganham de escolher
suas próprias aventuras. Esse tema costuma ser reforçado fazendo o Território
Selvagem ser particularmente letal, no sentido que decisões mal-pensadas muito
provavelmente levam a resultados desagradáveis.

Isso é tradicionalmente codificado no cenário na forma de **gradientes de
perigo**: quanto mais longe da Base e mais dentro do Território Selvagem, mais
sistematicamente perigoso são os monstros, os obstáculos, o clima, a geografia,
etc. Isso faz algum sentido pois, quanto mais longe, menos tocado pela
civilização é uma região e portanto mais perigos permanecem intactos nela. Isso
quer dizer que o Território Selvagem é dividido em **Regiões** e que cada
Região tem um **Nível de Desafio**. Isso simplifica o (não) balanceamento do
jogo: o mestre cria conteúdo APENAS com base no Nível de Desafio da Região onde
os jogadores estão explorando.

Ao mesmo tempo, existem **bolhas de perigo**, que são lugares específicos
dentro de uma região que apresentam desafios muito mais perigosos do que os da
região ao redor. Em geral, essas bolhas de perigo são isoladas e de difícil
acesso, indicando aos jogadores que talvez seja cedo demais para irem lá.

Em resumo, uma campanha *West Marches* deixa o controle do jogo na mão dos
jogadores, mas traz com essa liberdade o peso de consequências firmes e
porventura mortais – mas idealmente justas. A flexibilidade da campanha é
bastante adequada para mesas com horários instáveis, ajudando tanto jogadores
quanto mestres. Além disso, os mestres só precisam se preparar para aquilo os
jogadores anunciarem fazer antes da sessão.

## O Cenário

Você cometeu um crime dos piores possíveis. Seja com razão ou sem razão,
verdade ou não, o que importa é que você ofendeu os Reinos dos Povos Livres e
todos agora te querem morto. Como opção de sentença, você teve o direito de
optar pelo exílio no terrível continente de Morkbeth. Ainda parecia melhor do
que a morte, então você aceitou.

### O continente de Morkbeth

Séculos atrás, os Reinos dos Povos Livres tentou conquistar Morkbeth. Todos os
grandes líderes reuníram seus mais poderosos campeões e formaram a lendária
Legião da Conquista para domar Morkbeth de uma vez por todas.

Dez anos depois das frotas partirem apenas uma pessoa voltou em uma jangada
improvisada, coberta em um manto rasgado e mal conseguindo ficar de pé. Houve
grande disputa sobre a identidade da pessoa, as revelações que ela trouxera e,
mais importante, para onde ela foi depois disso. 

Uns diziam que era um dos campeões, em prantos e cheio de arrependimento por
ter deixado seus companheiros para trás mas que logo voltou correndo para o mar
em direção a Morkbeth para nunca mais ser visto novamente. Outros diziam que
não passava de um soldado qualquer à beira da insanidade, gritando que os
Reinos dos Povos Livres era uma grande sina perante as forças do mundo, e que
fora repentinamente capturado por celestiais vindos de uma fenda no céu, para
sempre levado ao esquecimento. Outros ainda diziam que a pessoa era na verdade
uma criatura maligna que veio para tirar sarro das tentativas dos mortais de
conquistar o inconquistável, e que sumiu em uma labareda verde e pútrida.

<img src='https://cdna.artstation.com/p/assets/images/images/003/401/958/large/tomas-honz-act1-finaldev.jpg?1473316621' style="width:100%"/>

A verdade não importava. O Reino dos Povos Livres sofrera uma profunda ferida
em seu orgulho e sua esperança, e nunca mais quiseram ouvir falar de Morkbeth.
O continente passou a ser um sinônimo de desgraça e morte. Não demorou muito
para que fosse usado como uma opção “misericordiosa” para aqueles indivíduos
que o Reino não queria mais lidar com. Desde então, toda pessoa sentenciada à
morte – não importa a raça ou origem - pode optar pelo exílio em Morkbeth, na
esperança de que o continente não faça jus às histórias que ouvira desde
criança.

Para ser levado a Morkbeth, os prisioneiros são mantidos dentro de contêineres
de madeira reforçada com magia para só abrir quando chegam à costa. Como parte
da farsa de “execução misericordiosa”, lhes é permitido levar um e apenas um
item, como uma arma ou um foco de magia.

### A Chegada

A viagem inteira, você ficou preso no escuro junto com outros exilados com
apenas o mínimo de comida oferecida diariamente. Um dia, vocês sentiram o
contêiner ser jogado ao mar e a comida parou de aparecer. Após algumas horas de
incerteza e desespero, um enorme tranco anunciou sua chegada à costa de
Morkbeth. Uma pequena porta se abriu para fora, e o que vocês viram foi...

