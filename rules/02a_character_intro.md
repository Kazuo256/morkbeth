
# Personagens

Você é um exilado de Morkbeth. Qualquer que seja sua origem dentre os Reinos do
Povos Livres, você fez algo no passado que causou a fúria de alguém poderoso o
suficiente para sancionar seu exílio. Agora, voltar não é mais uma opção. Sua
nova vida é nesse continente indomado e cheio de terrores de outrora.

Para padronizar e ajustar o jogo ao espírito de uma campanha *West Marches*,
esta seção introduz as “regras da casa” para criação e uso de personagens.

## Criação de personagens

Personagens devem ser criados usando as regras descritas a seguir. As fichas
devem ser feitas no [Dice Cloud](https://dicecloud.com/). [Este
link](https://docs.google.com/spreadsheets/d/1UfZ9e9Z9XFiZ7uapyDxrh9UskZSmPGa-nWr7mCqzevw/edit?usp=sharing)
contém uma série de material de apoio para usar o Dice Cloud, inclusive [um
gerador automático de fichas](https://andrew-zhu.com/dnd/dicecloudtools/).

