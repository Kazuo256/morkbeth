
# Aventuras

Morkbeth é diferente do mundo civilizado de onde você veio. Aqui, as criaturas
são mais vorazes e sagazes. Aqui, a maior parte das pessoas são a escória da
sociedade. Aqui, você não tem sequer a garantia de que vai conseguir voltar
para um lugar seguro no fim do dia.

Esta campanha conta com diversas revisões e adições às regras básicas de *D&D
5ª edição*. As regras aqui descritas se aplicam sobre todas as regras lançadas
em publicações oficiais (no caso, a última foi *Tasha's Cauldron of
Everything*). Além das regras oficiais, quando for o caso, deixaremos claro se
usamos uma regra opcional (por exemplo, *encumbrance*).

## Expedições

Aventuras nessa campanha são todas no formato de **expedição**: os heróis
partem de alguma base segura e se aventuram por território hostil até atingirem
o local almejado. Lá eles procuram qualquer que seja o objetivo da expedição, e
retornam depois, com ou sem sucesso. A única exceção costuma ser a primeira
aventura de personagens novos, em que acabaram de chegar à costa e ainda não
encontraram uma base de operações.

São os próprios jogadores que escolhem quando e onde cada aventura acontece.
Eles discutem para ver a disponibilidade de cada um e para onde seus
personagens gostariam de ir. Ao tomar uma decisão, o GM se dispõe a preparar e
mestrar as sessões necessárias. Só podem marcar uma aventura jogadores com uma
ficha de personagem pronta e aprovada, para que o mestre possa ter tempo de se
familiarizar com a equipe.

Toda sessão de uma aventura tem que terminar ou com o grupo fazendo um repouso
(curto ou longo), ou com o grupo voltando para uma das bases de operações.
Personagens sempre podem abandonar uma expedição entre sessões e, dependendo da
situação, personagens podem se juntar a uma expedição também entre sessões.
Vale notar que você só ganha XP pelas sessões que participou!

**No momento, estamos tendo apenas sessões remotas via
[Owlbear](https://www.owlbear.rodeo/)**.

Durante uma aventura existem quatro principais momentos distintos: a **viagem**
até o local da aventura, a **exploração** do local, eventuais **combates** e
**repouso**. Este capítulo descreve as regras da casa que usamos para esses
modos de jogo exceto combate — esse tem um [capítulo só para
ele]($root/$page_rules_combat.html).  Entre expedições os jogadores podem fazer
atividades de *downtime*, também descritas em um capítulo próprio. Se algo não
está explicado aqui, é porque muito provavelmente usamos as regras oficiais
nesse caso mesmo.

$begincolumn

## Viagem

Durante uma expedição em Morkbeth, os heróis precisam antes de mais nada chegar
ao local da aventura. Embora essa travessia não seja o foco do jogo, ela ainda
assim traz seus próprios desafios, que repercutem no resto da expedição. Toda
vez que os jogadores forem viajar de um lugar para outro, eles precisam:

1. Estabelecer destino
2. Determinar rota a seguir
3. Atribuir atividades
4. Eleger modo de viagem

Depois disso, o mestre irá determinar o resultado da viagem, o que foi gasto e
se houve alguma interrupção, como: esbarrar com um grupo de monstros, encontrar
um lugar suspeito, chegar ao destino, etc.

$endcolumn

$begincolumn

$vspace
$vspace

$begincard

#### Calendário de Morkbeth

A passagem do tempo em Morkbeth segue um calendário simplificado:

+ **Dias duram 24 horas**, como de costume
+ **Semanas têm apenas 5 dias**
+ **Um mês tem 6 semanas** (30 dias)
+ **Um ano tem 12 meses** (360 dias)

Cada mês corresponde a um ciclo lunar, sendo chamados de Primeira Lua,
Segunda Lua, etc. O ano começa no primeiro dia do Outono e cada estação dura
3 meses.

$endcard

$vspace
$vspace

$endcolumn

### Tempo de Viagem e Vigor

A principal forma que viagens influenciam a aventura é o tempo que os
personagens precisam gastar nelas. Mais tempo e mais esforço drenam recursos e
agravam os perigos em aventuras com urgência. O tempo em si é medido e regulado
usando a mecânica de Vigor:

$content{ $data|vigor }

Caso um grupo de criaturas tenham quantidades diferentes de Vigor, normalmente
basta considerar aquela que vai ficar sem vigor antes. Existem efeitos que
drenam pontos de Vigor de uma criatura. Além das mecânicas de Vigor, as
seguintes regras revisadas exercem um papel importante nas viagens:

$content{ $data|fatigue }

$content{ $data|encumbered }

$content{ $data|heavily_encumbered }

### Alimentação

Todo dia, os jogadores precisam comer e beber um mínimo de comida e água. Nós
usamos as seguintes alterações nas regras básicas:

$content{ $data|hunger }

$content{ $data|fresh_food }

Supõe-se que os personagens enchem seus cantis ou bebem água sempre que
possível durante uma viagem. O mestre avisará quando algo os impedir de ter
acesso a água. Quanto à comida, o que foi ingerido ao longo do dia é marcado ao
final do dia durante a cena de repouso, normalmente.

### Regiões e rotas

O segundo aspecto na preparação de uma viagem é determinar uma rota para
atravessar a região em que os personagens se encontram no momento. O continente
de Morkbeth é dividido em inúmeras regiões. Como explicado [na seção sobre
*Progressão*](#personagens-progressao), cada região tem um nível que determina
a dificuldade dos encontros e quanto experiência os jogadores ganham por
superar seus desafios.

Ao passar por uma região, os personagens geralmente querem chegar em algum
lugar, seja dentro dela ou além dos limites dela. É para isso que precisam
traçar uma **rota** dentro da região até tal ponto de interesse. Às vezes, as
regiões são navegáveis o suficiente ou o local de destino é evidente o bastante
para que seguir uma rota seja trivial.

Quando seguir uma rota é minimamente difícil, algum jogador precisa realizar a
atividade **Traçar Rota** para que seja possível encontrar o caminho certo. A
seção a seguir entra em mais detalhes sobre as atividades durante uma viagem.

### Modos de Viagem e Atividades

Quando os jogadores viajam, eles precisam escolher como vão se deslocar e quem
ficará a cargo de atividades auxiliares à jornada. **Cada personagem pode
realizar apenas uma atividade por vez**. De maneira geral, o modo de viagem
afeta o desempenho das atividades que, por sua vez, buscam trazer diversos
benefícios para os pergonagens.

Um fator importante nessas escolhas é o valor de Sabedoria (Percepção) passiva
dos jogadores, usado para detectar ameaças ou pontos de interesse. **Se uma
criatura realizar qualquer atividade durante a viagem, ela fica *SEM* Sabedoria
(Percepção) passiva**, e nunca poderá detectar nada (PHB 183). Isso significa
que, no caso de uma emboscada, por exemplo, a criatura ficaria automaticamente
surpresa.

Existem três tipos de modo de viagem: cauteloso, normal e acelerado. O modo
escolhido afeta a velocidade de marcha, o valor de Sabedoria (Percepção)
passiva (se houver) e os testes envolvidos nas atividades de viagem. A tabela
*Bônus e Penalidades por Modo de Viagem* detalha isso.  Em modo cauteloso os
personagens são furtivos o suficiente para poder fazer testes de Destreza
(Stealth) em grupo ao correr o risco de serem detectados.  Terreno difícil
reduz a marcha pela metade.

#### Bônus e Penalidades por Modo de Viagem

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Modo</th>
    <th>Marcha</th>
    <th>Percepção</th>
    <th>Atividades</th>
  </tr>
  $bonus_and_penalty_per_travel_mode[[
    <tr>
      <td>$mode</td>
      <td>$pace</td>
      <td>$pp</td>
      <td>$act</td>
    </tr>
  ]]
</table>

Como os jogadores podem variar entre sessões, pode ser que alguns personagens
saiam ou entrem em uma expedição entre uma sessão e outra. Nossas regras
aproveitam o fato que toda sessão acaba em um repouso para justificar a saída
e chegada desses personagens. Além disso, temos uma regra especial para
simplificar as viagens e permitir que todos joguem sempre que possível:

$content{ $data|auto_travel }

A seguir descrevemos as atividades mais comuns que se pode realizar durante uma
viagem, mas os jogadores podem propôr atividades diferentes das daqui, se
quiserem.

$content{ $data|navigate }

$content{ $data|mapping }

$content{ $data|seek }

## Exploração

Quando os aventureiros chegam em um local de interesse, começa a parte de
exploração da aventura. Costuma ser um momento onde os detalhes são mais
importantes, e a escala temporal pode ser medida na ordem de horas ou minutos.

### Tempo e Tensão

Quando o local explorado é perigoso, usamos a Pilha de Tensão (inventada pelo
[Angry GM](https://theangrygm.com/making-things-complicated/)). Esse sistema
serve para medir a passagem do tempo de forma que os jogadores não possam ficar
se distraindo para sempre.

A Pilha de Tensão consiste de um punhado de dados (todos d6) que vão sendo
acumulados conforme o tempo passa no jogo. Pode haver de zero a 6 dados nesse
monte. Cada dado corresponde a uma ação ou medida de tempo. Dependendo do que
os jogadores fizerem, é adicionado um dado à Pilha ou os dados que estão lá
são rolados. Às vezes ambos acontecem juntos.  Sempre que a Pilha for rolada e
aparecer algum 1, acontece uma **complicação**. De maneira geral, a Pilha
aumenta quando os jogadores gastam tempo e ela é rolada quando eles fazem algo
arriscado. Quando a Pilha fica cheia ela é imediatamente rolada e depois
esvaziada.

Em geral, cada d6 representa 1 hora ou 10 minutos, dependendo do local
explorado. Um pântano requer horas de movimentação. Por outro lado, mover-se
entre salas de uma tumba leva apenas alguns minutos.  Quando os d6s equivalem a
1 hora, essas horas consomem vigor como durante uma viagem, dada a proporção
das distâncias que estarão percorrendo. No entanto, a marcha é sempre em modo
cauteloso pois os heróis permanecem mais atentos aos detalhes, oportunidades e
perigos.

A seguir descrevemos algumas ações comuns durante a parte de exploração de uma
aventura. Sempre é possível realizar ações diferentes dessas, assim como é
possível adaptar outras ações para se encaixar em uma dessas.

$content{ $data|change_places }

$content{ $data|comb }

$content{ $data|harvest }

### Iluminação e visibilidade

É bastante comum aventureiros explorarem calabouços, masmorras. cavernas e
outros ambientes a luminosidade é limitada. Para simplificar o processo de
determinar o que é visível ou não, adotamos algumas variações de regras do
*Giffyplyph's Darker Dungeons*.

Cada sala ou ambiente em um local fechado possui apenas um único nível geral de
iluminação de acordo com a tabela *Níveis de Iluminação*. **Vale notar que em
iluminação *darkest* nem criaturas com *darkvision* conseguem enxergar**.

Fontes de luz contribuem para elevar esse nível de iluminação dependendo da sua
intensidade segundo as tabelas de *Fontes de Luz*. Magias que produzem luz em
geral são consideradas *small* salvo as exceções na tabela *Magias Muito
Luminosas*.

No entanto, o tamanho da sala também influencia quanto cada fonte de luz
contribui para o nível total de luminosidade, como indica a tabela *Escala de
Salas*. Em salas ou ambientes muito grandes, caso precisamos determinar a
luminosidade próxima aos heróis, consideraremos o nível de iluminação total
apenas da área em um raio de 60 ft. em torno deles (um espaço *large*).

Vejamos um exemplo. Em uma sala *large* com nível de luminoside 1 (*dark*),
uma tocha (fonte de luz *small*) aumenta o nível apenas para 1.5, sendo
insuficiente para melhorar a visiblidade. Seriam preciso pelo menos duas tochas
para fazer essa sala ficar com luminosidade 2 (*dim*) e quatro para chegar em
3 (*bright*).

$begincolumn

#### Níveis de Iluminação

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Nível</th>
    <th>Iluminação</th>
    <th>Descrição</th>
  </tr>
  $light_levels[[
    <tr>
      <td>$lv</td>
      <td>$light</td>
      <td>$info</td>
    </tr>
  ]]
</table>

#### Escala de Salas

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Tamanho</th>
    <th>Diâmetro Máx.</th>
    <th>Ajuste</th>
  </tr>
  $room_sizes[[
    <tr>
      <td>$size</td>
      <td>$diameter</td>
      <td>$mult</td>
    </tr>
  ]]
</table>

$endcolumn

$begincolumn

#### Fontes de Luz

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Tamanho</th>
    <th>Nível</th>
    <th>Exemplos</th>
  </tr>
  $sources_of_light[[
    <tr>
      <td>$size</td>
      <td>$lv</td>
      <td>$ex</td>
    </tr>
  ]]
</table>

<span style="display: block; height: 0.9em" class="w3-hide-small"></span>

#### Magias Muito Luminosas

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Tamanho</th>
    <th>Magias</th>
  </tr>
  $magic_sources[[
    <tr>
      <td>$size</td>
      <td>$spells</td>
    </tr>
  ]]
</table>

$endcolumn

## Repouso

Saber descansar para se recuperar dos desafios de Morkbeth é um dos principais
fatores para garantir expedições bem sucedidas. No entanto, é difícil descansar
quando se está no meio das terras indomadas do exílio. Nesta campanha, repousos
curtos funcionam normalmente mas repousos longos são bem mais limitados. Em
compensação, tem um novo tipo de repouso: o repouso estendido. Essa nova
modalidade de repouso traz também a regra de Fortaçecer Laços, que permite os
jogadores usarem sua criatividade para recuperar recursos do grupo em troca da
promessa de uma jogada com vantagem no futuro.

$content{ $data|long_rest }

$content{ $data|extended_rest }

$content{ $data|bonding }

