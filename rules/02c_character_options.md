
# Opções na Criação do Personagem

Todas as raças, sub-raças, classes, arquétipos, *backgrounds*, talentos e
magias publicadas em material oficial da Wizards of the Coast são válidas. **Na
dúvida, consulte o DM antes.**

Você pode alterar a história, proficiências, característica de personalidade,
ideal, laço e defeito do seu *background* à vontade, mas precisa usar as
características (*feature*) de um *background* oficial. **Além disso, seu
background precisa incluir o crime (ou não-crime) que levou seu personagem a
ser exilado.**

Lembre-se que **você começa com apenas um item, que pode ser um dado pela
classe ou pelo *background* **.

Algumas das regras usadas nessa campanha mudam drasticamente o balanceamento do
jogo. Portanto, se você gosta de otimizar seus personagens, recomendamos ver as
demais regras antes de montar sua ficha. Em particular, note que:

+ Testes de iniciativa somam tanto seus modificadores de DEX quanto de INT;

+ Usamos as regras variantes de carga, mais regras especiais da casa;

+ Escudos podem ser usados para reduzir dano em troca de uma chance de quebrar;

+ INT e CHA tem utilidades explícitas em combate;

+ Armas com *finesse* só podem usar DEX.

Por fim, algumas características, itens e magias sofrem as seguintes
alterações.

$content{ $data|tactical_wit }

$content{ $data|wanderer }

$content{ $data|bag_of_holding }

$content{ $data|portable_hole }
