
# Combate

Confrontos durante uma aventura são praticamente inevitáveis e, em Morkbeth,
toda vantagem tática faz a diferença. Usamos uma série de regras modificadas e
adicionais para enriquecer a experiência de combate. Se você gosta de otimizar
seus personagens, preste atenção pois muitas estratégias convencionais deixam
de valer a pena e outras, menos utilizadas, podem ser mais interessantes.

## Iniciativa

Quando um combate começa e as criaturas precisam determinar sua iniciativa,
você usa o teste de habilidade *que fizer mais sentido na circunstâncias*.
Agir primeiro é Destreza, mas notar perigo é Sabedoria (Percepção), fazer uma
investida é Força (Atletismo), e reconhecer uma presença mágica é Inteligência
(Arcana), por exemplo. Note que isso permite aplicar bônus de proficiência na
iniciativa.

## Conhecendo seu inimigo

Sempre que fizer sentido, criaturas em combate terão uma ou mais informações
úteis sobre si que os jogadores podem descobrir usando testes de Inteligência
passiva aplicando proficiências diversas. Isso reflete o conhecimento prévio
que os personagens têm sobre a espécie, cultura, equipamento, técnicas, e
qualquer outra proficiência que possa dizer algo sobre a criatura. Por exemplo,
com Inteligência (Adestrar Animais) passiva de 15, é possível saber como
*Gratters* costumam trabalhar em bando para caçar suas presas. Isso não quer
dizer que esse é o único jeito de descobrir essas informações — mas é com
certeza o menos dolorido!

As proficiências que normalmente se aplicam para esse tipo de teste são:
Arcana, História, Investigação, Natureza, Religião, Adestrar Animais, Medicina
e Sobrevivência. A proficiência exata depende da criatura e da informação em
si.

## Novas Ações

As ações a seguir dão maior variedade nos turnos de combate, principalmente
para classes de combate físico mas também para personagens com foco em
inteligência e carisma. Sempre que ficar um turno sem o que fazer, lembre-se
dessas opções!

$content{ $data|charged_blow }

$content{ $data|disarm }

$content{ $data|distract }

$content{ $data|intercept }

$content{ $data|shove }

### Movimento

Diferente do que está escrito em PHB 191, você pode se mover através do espaço
ocupado por uma criatura hostil de tamanho próximo se usar uma ação bonus e
explicar como você faz isso. Exemplos comuns são tentar trombar com ela ou
manobrar de maneira a evitar contato com ela, o que envolve testes de
habilidade.

## Condições

Aqui descrevemos alterações nas regras de algumas condições usadas no cenário
de Morkbeth, assim como novas condições.

$content{ $data|frightened }

$content{ $data|prone }

$content{ $data|restrained }

$content{ $data|stunned }

$content{ $data|unsteady }

$content{ $data|sluggish }

$content{ $data|slowed }

## Sequelas e morte

A morte em Morkbeth está sempre à espreita. Cair em combate tem consequências
bem mais severas do que de costume.

$content{ $data|deep_wound }

$content{ $data|death_mark }

