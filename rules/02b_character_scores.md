
# Pontos de Habilidade

Os pontos de habilidade (*ability scores*) base são definidos usando a variação
de regras “point-buy” descritas no PHB 13 (ilustrada [nesta
calculadora](https://5e.tools/statgen.html#pointbuy), por exemplo). Além disso,
nessa campanha pontos extras de habilidade dados por raça são diluidos para
permitir uma variedade maior de combinação de raças e classes. **Ao invés de
ganhar os pontos extras especificados nas raças, você ganha**:

* Um ponto em qualquer habilidade que sua raça e subraça originalmente dariam
  bônus;

* Um ponto em uma das duas habilidades determinadas pelo seu *background* como
  descrito na tabela *Pontos Extras por Background*;

* Um ponto em qualquer habilidade, contanto que os três pontos aqui dados não
  fiquem todos na mesma habilidade.

Caso seu *background* não esteja presente no PHB, consulte o mestre para
determinar em quais habilidades você pode ganhar pontos extras.

#### Pontos Extras por Background

$begincolumn

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Background</th>
    <th>Ponto Extra</th>
  </tr>
  $asi_by_background_1[[<tr><td>$bg</td><td>$asi</td></tr>]]
</table>

$endcolumn

$begincolumn

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Background</th>
    <th>Ponto Extra</th>
  </tr>
  $asi_by_background_2[[<tr><td>$bg</td><td>$asi</td></tr>]]
</table>

$endcolumn

<span style="height:2em"></span>
