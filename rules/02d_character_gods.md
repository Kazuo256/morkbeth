
# Deuses e Divindades

Há um panteão de deuses LN que mais ou menos se coordenam para que o multiverso
não entre em colapso:

+ The Flame (light)

+ The Earth (nature)

+ The Eye (knowledge)

+ The Heart (life)

+ The Bone (death)

+ The Hammer (war)

Tem um deus primordial CN, o mais antigo dos deuses, que controla as forças
mais brutas pré-criação:

+ The Storm (tempest)

E um deus CN que quer derrubar o panteão para dar liberdade verdadeira a todos:

+ The Voice (trickery)

