
# Equipamento

Em um continente com praticamente nenhuma forma de civilização, todo e qualquer
equipamento é de imenso valor. Grande parte das aventuras em Morkbeth giram em
torno de encontrar equipamentos ou materiais que possam ser usados para
manufaturá-los. Equipamentos, em particular, oferecem diversas opções táticas
para os personagens. Aqui estão descritas alterações em mecânicas antigas e
introdução de algumas mecânicas novas envolvendo armas e escudos.

## Armas

As propriedades e mecânicas a seguir complementam as regras de armas e combate
apresentadas no PHB.

$content{ $data|focused_strike }

$content{ $data|loading }

$content{ $data|finesse }

$content{ $data|pound }

$content{ $data|cleave }

$content{ $data|pin }

$content{ $data|bladelock }

$content{ $data|deadly }

### Escudos

As qualidades e mecânicas a seguir complementam as regras de escudos e combate
apresentadas no PHB.

$content{ $data|sturdiness_die }

$content{ $data|shield_block }

$content{ $data|small_shield }

$content{ $data|large_shield }

