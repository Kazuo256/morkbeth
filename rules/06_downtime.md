
# Tempo Livre

$begincolumn

Entre as aventuras, os personagens dos jogadores ficam seguros em uma das
cidades aliadas, podendo fazer Repousos Longos e outras atividades. Também é
possível recorrer à população das cidades aliadas para expandir as cidades ou
ir atrás de oportunidades especiais.

## Atividades Semanais

Cada semana, você pode fazer apenas uma das atividades descritas a seguir. Você
escolhe as atividades que vai realizar após agendar uma aventura, isto é, suas
atividades são sempre a "preparação" do personagem para uma aventura. Lembre-se
que semanas nessa campanha têm apenas 5 dias.

$endcolumn

$begincolumn

$vspace

##### Níveis de Condição de Vida

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Condição</th>
    <th>Custo/Semana</th>
    <th>Exaustão</th>
  </tr>
  $lifestyles[[
    <tr>
      <td>$style</td>
      <td>$cost</td>
      <td>$exhaustion</td>
    </tr>
  ]]
</table>

$vspace

$vspace

$endcolumn

Independente da atividade que você fizer, cada semana passada em Sharpridge,
exceto a primeira após uma aventura, requer gastos básicos de sobrevivência. As
consequências e os custos de cada nível de condição de vida estão detalhados na
tabela *Níveis de Condição de Vida*.

$content{ $data|rest_activity }

$content{ $data|subsist }

$content{ $data|crafting }

$content{ $data|animal_care }

$content{ $data|sacred_ritual }

$content{ $data|magic_studies }

$content{ $data|preventive_research }

## Pontos de Expansão

Sharpridge é um estabelecimento recente e em constante crescimento. Conforme
mais exilados aderem à causa, maiores as possibilidades de serviços e expansão.
Como parte dos mais fortes guerreiros da cidade, é geralmente responsabilidade
dos jogadores trazerem mais exilados para a causa.

Cada 10 pessoas que os aventureiros trazem para Sharpridge conferem *a cada um
dos jogadores* 1 **ponto de expansão** (inclusive retroativamente). Esses
pontos podem ser gastos para habilitar, melhorar ou realizar serviços
especiais — em outras palavras, *expandir* as capacidades das cidades livres
— e para ganhar conquistas. A contagem atual dos pontos pode ser consultada na
seção de [Expansões e Conquistas](/morkbeth/info/expansions.html).

### Expansões

$content{ $data|gear_and_tools }

$content{ $data|craftsmen }

$content{ $data|library }

$content{ $data|scavengers }

$content{ $data|outposts_and_roads }

### Conquistas

Quando os aventureiros interagem com pessoas de origens diversas e juntam isso
à própria experiência adquirida durante as expedições, inúmeras possibilidades
de crescimento dos personagens surgem. Seja um espachim de um estilo obscuro,
um mago com o qual é possível discutir novas teorias ou simplesmente uma
senhora com histórias sobre mitos e lendas há muito esquecidos, esses
especialistas permitem você melhorar características do seu pergonagem. Toda
conquista exige uma semana de dedicação.

***Talentos***. Um personagem seu pode ganhar um talento (*feat*) para o qual
ele ou ela se qualifique (e que não esteja vetado pelo mestre). O primeiro
talento custa 5 pontos de expansão e cada outro talento depois do primeiro
custa 5 pontos a mais que o anterior.

***Magias únicas***. Se seu personagem sabe usar magia, você pode escolher uma
magia que você consegue usar e formular uma *variação única* junto com o
mestre. A magia nova pode ser de um nível diferente da original, contanto que
você possa usá-la. Alternativamente, se for um mago, você pode adicionar uma
magia nova da lista de magias de mago ao seu grimório. O custo em pontos de
expansão para ambos os casos é duas vezes o nível da magia final.

