
# Histórico de Versões

### 3.0.0

+ Regras movidas para a página on-line da campanha
+ Aventuras
	+ Regras de combate movido para capítulo separado
	+ Pequeno ajuste nas regras de viagem
	+ Mecânicas de viagem organizadas para facilitar consulta
	+ Aventuras podem durar mais de uma sessão, mas precisam parar em um short
		ou long rest
  + Personagens podem sair ou se juntar a uma expedição contanto que faça algum
		sentido
+ Novas receitas
  + *Quiver of Ehlonna*
  + *Pact of the Rod Keeper +1*
+ Manufatura
  + Remoção da propriedade *fragile*
  + Ossos deixam de ter proteção pesada e viram só proteção leve
  + Modificadores simplificados
  + Materiais únicos
+ Itens
  + Não existem mais *Potions of Recovery*. Se você tinha uma, ela se torna uma
    Poção de Cura ou uma Poção do Poder de nível correspondente.
  + Não existem mais *Potions of Stamina*. Se você tinha uma, ela se torna uma
    Poção de Cura ou uma Poção do Poder de nível correspondente.
  + Cristais do Retorno causam exaustão mas podem ser feitos usando apenas uma
    gema rara.

### 2.0.0 (05 / 03 / 2021)

+ Revisão do capítulo *Aventuras*
  + Oficialização das regras de Vigor e Fadiga
  + Fome causa Fadiga
  + Esclarecimento das regras de viagem
  + Remoção de tensão durante viagens
  + Calendário de Morkbeth
  + Re-ordenação das seções
  + Simplificação das atividades de viagem
  + Ações de exploração
  + Coletar material agora é uma ação de 1 hora
  + Nova condição: *unbalanced*
  + Ação *Distract* afeta percepção
  + Ação *Shove* pode deixar *unbalanced*
+ Atividades
  + Custos de vida aumentaram 
  + Aumento do custo para estudar magia
  + Melhorias no texto dos treinamentos
  + Treinamento com dano concussivo deixa *unbalanced*
  + Pesquisas preventivas
+ Compras e serviços
  + Veículos e montarias à venda
  + Serviços liberados
  + Aliados oficializados
  + Materiais incomuns à venda
+ Expansões e Conquistas
  + Não tem mais "caça aos monstros" 
  + Rotas dispensam tempo de entrega dos catadores
  + Conquistas: ganhe feats e magias alternativas!
+ Equipamentos
  + Pequena revisão em *focused strikes*

### 1.6.0 (16 / 10 / 2020)

+ Mudança nas regras de *sturdiness dice*
+ Ajuste no valor dos materiais
+ Diversos ajustes nas receitas de manufatura
+ Testes em atividades de exploração foram flexibilizados
+ Poções incomuns ou mais raras não têm mais preço estabelecido
+ Removidas as partes que agora ficam na planilha "Diários dos Exilados de Morkbeth"

### 1.5.9 (02 / 10 / 2020)

+ Novo PC: Væ´zrik
+ Revisão das mecânicas *find opening*, *support* e *pin*
+ Receita esquecida: *blowgun*

### 1.5.8 (14 / 9 / 2020)

+ Atualização do diário
+ Novos PCs: Nameless, Crow, Blue, Mofélia e Arry
+ Nova atividade de downtime: estudos em magia
+ Pequeno ajuste na expansão "escritura sagrada"

### 1.5.7 (31 / 8 / 2020)

+ Atualização do diário, agora com notícias entre aventuras
+ Atualização dos rumores
+ Receita para *slippers of spider climbing*
+ Receita para *sending stones*
+ Item novo: ciphers (com receita até nível 3)

### 1.5.6 (22 / 8 / 2020)

+ Atualização do diário
+ Ajustes em *heed caution*
+ Ajustes em *powerful strike*
+ Receita para *bracers of defense*

### 1.5.5 (15 / 8 / 2020)

+ Novo item mágico: potions of stamina
+ Novas fórmulas:
  + Potions of climbing
  + Potions of fire breath
  + Potions of stamina
+ Conversão de materiais

### 1.5.4 (1 / 8 / 2020)

+ Novo rumor: mistério de Gurrigham
+ Diário atualizado
+ Tabela de materiais por região atualizada

### 1.5.3 (16 / 7 / 2020)

+ Novo rumor: um lugar sombrio
+ Nova seção no diário: pesquisas na biblioteca
+ Diário atualizado

### 1.5.2 (28 / 6 / 2020)

+ Atualizações no diário

### 1.5.1 (16 / 6 / 2020)

+ Seção no diário para os progressos de Sharpridge

### 1.5.0 (10 / 6 / 2020)

+ Novas opções de expansão para Sharpridge:
  + Biblioteca
  + Catadores
  + Postos avançados e estradas
+ Novo item mágico: *Crystal of Return*
+ Modificador renomeado: *Healing* para *Holy*
+ Tabela de Materiais por Região

### 1.4.2 (7 / 6 / 2020)

+ Rumores atualizados
+ Novas fórmulas:
  + Bateria pro Artefato da Conquista (em rumores)
  + *Cloak of Protection*
  + *Gauntlets of Ogre Power*
  + *Climber's kit*
+ Novo NPC: Lenolaen
+ Revisão das regras para materiais com raridade maior

### 1.4.1 (29 / 5 / 2020)

+ Novo personagem de jogador: Anilee (brocoli)
+ Diário dos Exilados atualizado

### 1.4.0 (15 / 5 / 2020)

+ Revisão das regras de marcha de viagem e *encumbrance*
+ Novo rumor: vultos no crepúsculo
+ Expandindo de Sharpridge: nova opção de *downtime*

+ Fórmulas novas e ajustadas:
  + Goggles of Night Vision
  + Potions of Healing (e variantes)
  + Potions of Recovery, Resistance, e Spellpower
+ Datas no diário corrigidas

### 1.3.2 (3 / 5 / 2020)

+ Mapa dos arrededores de Sharpridge, cortesia de Ponko

### 1.3.1 (2 / 5 / 2020)

+ Typos
+ Link para ficha compartilhada de Sharpridge
+ Nova fórmula: *ring of spell storing*

### 1.3.0 (1 / 5 / 2020)

+ Adição das [Advanced Rules](https://docs.google.com/document/d/1pIX7doAIm20TSDni2Rk7cHqRB32loNVDTEnLVakG_YA/edit?usp=sharing)
+ Novo índice melhor formatado
+ Capítulo 1
  + Detalhes sobre ASIs
  + Classes, raças, backgrounds, spells, e itens modificados
+ Capítulo 2
  + Completamente reorganizado
  + Regras de deslocamento, exploração, combate, condições e repouso
+ Capítulo 3
  + Novas atividades de downtime
  + Novos rumores e NPCs
+ Capítulo 4
  + Mecânicas do Advanced Rules
  + Todos os modificadores de materiais
  + Novos itens mágicos
  + Mais fórmulas de itens mágicos

### 1.2.5 (26 / 4 / 2020)

+ Escudos só rolam *sturdiness die* se o erro for no máximo igual ao bônus que eles conferem
+ Reformatação do capítulo de Equipamentos
+ Atualização do índice
+ Ajustes nas condições para realizar um *Charged Blow*

### 1.2.4 (25 / 4 / 2020)

+ Mudanças em modificadores de material
  + Novos: sharp, fine, cyan e amber
  + Renomeado: fiery -> crimson
+ Mudanças em NPCs de Sharpridge:
  + Rabab (atualizado)
  + Ponko (novo)
+ Ferramentas e suprimentos de até 25gp estão agora disponíveis em Sharpridge
+ Novo personagem de jogador: Rhogar (Guilherme)
+ Novo capítulo: [Diário dos Exilados](#diario-dos-exilados)

### 1.2.3 (29 / 3 / 2020)

+ Esclarecimento sobre manufatura de itens mágicos

### 1.2.2 (27 / 3 / 2020)

+ Fórmula para *alchemy jug*
+ Novo modificador de material: *dimensional*
+ Novo rumor: "desaparecido"

### 1.2.1 (1 / 3 / 2020)

+ *Structure (conductive)* menos punitivo
+ Regras sobre armazenamento de itens
+ Alguns modificadores de materiais

### 1.2.0 (1 / 3 / 2020)

+ Novo capítulo: [Sharpridge](#sharpridge)
  + Novos rumores
    + Flores medicinais
    + Gruta assombrada
    + Travessia do Rio Cobuton
    + Regras para atividades em downtime
  + Informações básicas sobre NPCs
+ Novo capítulo: [Equipamentos](#equipamentos)
  + Novas regras de equipamentos e combate
  + Regras para manufatura
+ Índice!

### 1.1.1 (14 / 2 / 2020)

+ Percepção passiva "colaborativa"
+ Quantidade de alimento fresco condicionado ao ambiente
+ Novos personagens de jogador: Trunkstar (Maki) e 14 (Ryu)

### 1.1.0 (9 / 2 / 2020)

+ Mudei de novo as regras de passagem do tempo durante expedições
+ Acrescentei regras para navegação e exploração, incluindo:
  + Obstáculos comuns (só rios)
  + Providenciando alimento fresco
+ Novo personagem de jogador: "King" Arthur (Cabeça)

### 1.0.2 (3 / 2 / 2020)

+ Mudei um pouco as regras de passagem de tempo durante expedições

### 1.0.1 (30 / 1 / 2020)

+ Corrigi todas as menções antigas a Morkelbeth para Morkbeth
+ Corrigi alguns typos
+ Novo personagem de jogador: Taj (Tui)

### 1.0.0 (28 / 1 / 2020)

Primeira versão!
