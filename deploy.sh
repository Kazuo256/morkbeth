#!/bin/bash

# Install Lua 5.1.5
curl -R -O http://www.lua.org/ftp/lua-5.1.5.tar.gz
tar -zxf lua-5.1.5.tar.gz
pushd lua-5.1.5
make linux test
make install
popd

lua -v

# Install Luarocks 3.7.0
wget https://luarocks.org/releases/luarocks-3.7.0.tar.gz
tar zxpf luarocks-3.7.0.tar.gz
pushd luarocks-3.7.0
./configure --with-lua-include=/usr/local/include
make
make install
popd

luarocks --version

luarocks install sitegen

sitegen

ls -Rla

