
local lines = {}
for line in io.lines() do
  lines[#lines + 1] = line
end

local function split_cells(line)
  local cells = {}
  for cell in line:gmatch "[^|]+" do
    cell = cell:gsub("^ +", ""):gsub(" +$", "")
    cells[#cells + 1] = string.format("\"%s\"", cell)
  end
  return cells
end

local headers = split_cells(lines[1])

local headers_fmt = [[
    headers: { %s }]]

print(headers_fmt:format(table.concat(headers, ", ")))

local rows = {}

local row_fmt = "{ %s }"

for i = 3, #lines do
  local line = lines[i]
  local cells = split_cells(line)

  local row = row_fmt:format(table.concat(cells, ", "))

  rows[#rows + 1] = row
end

local content_fmt = [[
    rows: {
      %s
    }]]

print(content_fmt:format(table.concat(rows, "\n      ")))

