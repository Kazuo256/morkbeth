keywords = {
  -- Equipamentos
  sturdiness_die: "dado de robustez"
  -- Material properties
  structure: "estrutura"
  harm: "dano"
  protection: "proteção"
  consummable: "consumível"
  valuable: "valioso"
  dense: "denso"
}

=>
  for id, keyword in pairs(keywords)
    @[id] = "<code>keyword</code>"
