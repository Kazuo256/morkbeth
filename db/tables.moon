
xp_award_per_region_level = {
  { lv: 1,  half: 25,   },
  { lv: 2,  half: 50,   },
  { lv: 3,  half: 75,   },
  { lv: 4,  half: 125,  },
  { lv: 5,  half: 250,  },
  { lv: 6,  half: 300,  },
  { lv: 7,  half: 375,  },
  { lv: 8,  half: 450,  },
  { lv: 9,  half: 550,  },
  { lv: 10, half: 600,  },
  { lv: 11, half: 800,  },
  { lv: 12, half: 1000, },
  { lv: 13, half: 1100, },
  { lv: 14, half: 1250, },
  { lv: 15, half: 1400, },
  { lv: 16, half: 1600, },
  { lv: 17, half: 1950, },
  { lv: 18, half: 2100, },
  { lv: 19, half: 2450, },
  { lv: 20, half: 2850, },
}

for _, row in ipairs xp_award_per_region_level
  row.full = row.half * 2
  row.double = row.full * 2

{
  stuff: {
    { bg: "Athletic",       asi: "STR or DEX" },
    { bg: "Brute",          asi: "STR or CON" },
    { bg: "Enforcer",       asi: "STR or INT" },
    { bg: "Warden",         asi: "STR or WIS" },
    { bg: "Leader",         asi: "STR or CHA" },
    { bg: "Resilient",      asi: "DEX or CON" },
    { bg: "Swift",          asi: "DEX or INT" },
    { bg: "Pursuer",        asi: "DEX or WIS" },
    { bg: "Gracious",       asi: "DEX or CHA" },
    { bg: "Opulent",        asi: "CON or INT" },
    { bg: "Survivor",       asi: "CON or WIS" },
    { bg: "Vigorous",       asi: "CON or CHA" },
    { bg: "Sharp",          asi: "INT or WIS" },
    { bg: "Eloquent",       asi: "INT or CHA" },
    { bg: "Advisor",        asi: "WIS or CHA" },
  },
  asi_by_background_1: {
    { bg: "Acolyte",        asi: "INT ou WIS" },
    { bg: "Charlatan",      asi: "WIS or CHA" },
    { bg: "Criminal",       asi: "CON or INT" },
    { bg: "Entertainer",    asi: "DEX or CHA" },
    { bg: "Folk Hero",      asi: "STR or CHA" },
    { bg: "Guild Artisan",  asi: "INT or CHA" },
    { bg: "Hermit",         asi: "CON or WIS" },
  },
  asi_by_background_2: {
    { bg: "Noble",          asi: "INT or CHA" },
    { bg: "Outlander",      asi: "STR or CON" },
    { bg: "Sage",           asi: "INT or WIS" },
    { bg: "Sailor",         asi: "DEX or CON" },
    { bg: "Soldier",        asi: "STR or DEX" },
    { bg: "Urchin",         asi: "DEX or INT" },
  },
  xp_award_per_region_level: xp_award_per_region_level
  bonus_and_penalty_per_travel_mode: {
    { mode: "Cauteloso",  pace: "1 mi/h", pp: "+5", act: "Vantagem" },
    { mode: "Normal",     pace: "2 mi/h", pp: "+0", act: "---" },
    { mode: "Acelerado",  pace: "3 mi/h", pp: "-5", act: "Desvantagem" },
  }
  light_levels: {
    { lv: 0, light: "Darkest",    info: "Ausência total de luz" },
    { lv: 1, light: "Dark",       info: "Apenas resquícios de luz" },
    { lv: 2, light: "Dim",        info: "Parcialmente iluminado" },
    { lv: 3, light: "Bright",     info: "Iluminação abundante" },
    { lv: 4, light: "Brightest",  info: "Iluminação absoluta" },
  }
  sources_of_light: {
    { size: "Tiny",      lv: "+0.2",ex: "Vela" },
    { size: "Small",     lv: "+1",  ex: "Tocha, lanterna (coberta), sinalizador" },
    { size: "Medium",    lv: "+2",  ex: "Lanterna (descoberta), fogueira" },
    { size: "Large",     lv: "+4",  ex: "Braseiro, candelabro" },
    { size: "Huge",      lv: "+8",  ex: "Elemental de fogo" },
    { size: "Gargantuan",lv: "+16", ex: "Prédio em chamas" },
  }
  magic_sources: {
    { size: "Medium", spells: "Daylight, flame shield, wall of fire" },
    { size: "Large",  spells: "Dawn, investiture of flame, wall of light" }
  }
  room_sizes: {
    { size: "Tiny",       diameter: "10 ft.",  mult: "x 4" },
    { size: "Small",      diameter: "30 ft.",  mult: "x 2" },
    { size: "Medium",     diameter: "60 ft.",  mult: "x 1" },
    { size: "Large",      diameter: "120 ft.", mult: "x 1/2" },
    { size: "Huge",       diameter: "240 ft.", mult: "x 1/4" },
    { size: "Gargantuan", diameter: "---",     mult: "x 1/8" },
  }
  lifestyles: {
    { style: "Miserável",   cost: "---", exhaustion: "+2 (até 4)" },
    { style: "Insalubre",   cost: "5 sp", exhaustion: "+2 (até 3)" },
    { style: "Pobre",       cost: "1 gp", exhaustion: "+1 (até 3)" },
    { style: "Modesto",     cost: "5 gp", exhaustion: "0" },
    { style: "Confortável", cost: "10 gp", exhaustion: "-1" },
  }
  training_options: {
    headers: { "Tipo de Equipamento", "Circunstância", "Efeito" },
    rows: {
      { "Armadura Leve", "Testes de resistência de Destreza", "Vantagem" },
      { "Armadura Média", "Movimento", "5 ft. adicionais que não desencadeam</br>Ataques de Oportunidade" },
      { "Armadura Pesada", "Ser atacado à distância", "Atacante tem desvantagem" },
      { "Arma Concussiva", "Acertar ataque", "Opcionalmente, causa metade do dano</br> mas deixe o alvo Desequilibrado" },
      { "Arma Sutil", "Receber dano de arma<br/>corpo-a-corpo", "Opcionalmente, use sua reação para<br/> atacar com sua arma sutil" },
      { "Arma Perfurante", "Acertar ataque em criatura<br/>com armadura pesada", "Adiciona 1d6 ao dano" },
      { "Arma com Alcance", "Acertar ataque", "Opcionalmente, mova a criatura atingida<br/> 5 ft. em qualquer direção mas sem sair<br/> do seu alcance; não causa Ataque de<br/>Oportunidade" },
      { "Escudo", "Acertar ataque com<br/>arma corpo-a-corpo", "Opcionalmente, empurre o alvo 5 ft. para<br/> longe; não causa Ataque de Oportunidade" },
      { "Arma Cortante", "Acartar ataque em criatura<br/>com armadura leve, natural,<br/>ou sem armadura", "Adiciona 1d6 ao dano" },
      { "Arma Versátil", "Empunhar com duas mãos", "Adiciona +1 AC" },
    }
  }
  expansion_points: {
    headers: { "Exilados recrutados", "Pontos de expansão por jogador" },
    table_class: "w3-centered w3-twothird",
    rows: {
      { 165, 16 }
    }
  },
  expansion_points_distribution: {
    headers: { "Jogador", "Pontos usados", "Pontos disponíveis" },
    table_class: "w3-half",
    column_class: { "", "w3-center", "w3-center" },
    rows: {
      { "Tui",      8, 8 },
      { "Cabeça",   7, 9 },
      { "Ryu",      9, 7 },
      { "Maki",     3, 13 },
      { "Rafael",   4, 12 },
      { "Matheus",  5, 11 },
      { "Ivan",     5, 11 },
    }
  },
  gear_expansions: {
    headers: { "Valor máximo", "Pontos" },
    rows: {
      { "<s>Até 50 gp</s>", 1  },
      { "Até 100 gp", 2  },
      { "Até 500 gp", 5  },
      { "Até 1000 gp", 10 },
      { "Até 10000 gp", 15 },
      { "Sem limite", 20 },
    }
  },
  crafting_expansions: {
    headers: { "Raridade dos materiais", "Pontos" },
    rows: {
      { "<s>Comum</s>",   1  },
      { "<s>Incomum</s>", 5  },
      { "Raro",           10 },
      { "Muito Raro",     25 },
      { "Lendário",       50 },
    }
  }
  library_expansions: {
    headers: { "Raridade dos materiais", "Pontos", "Custo por pesquisa" },
    rows: {
      { "<s>Comum</s>",   1,  "5 gp" },
      { "<s>Incomum</s>", 4,  "50 gp" },
      { "Raro",           8,  "500 gp" },
      { "Muito Raro",     12, "5000 gp" },
      { "Lendário",       16, "50000 gp" },
    }
  }
  territory_expansions: {
    headers: { "Expansão", "Pontos", "Preço", "Semanas" },
    rows: {
      { "Posto avançado", 10, "1000 gp", 8 }
      { "Rota nova"     , 2,  "100 gp" , 2 }
    }
  }
  material_types: {
    headers: { "Nome", "Valor", "Peso", "Propriedades" }
    title: "Tipos de materiais"
    table_class: "w3-twothird",
    rows: {
      { "Madeira, Tora de",       "1 gp",   "10 lb.",
        "Estrutura (rígida), Proteção (leve)" }
      { "Ossos, Pilha de",        "1 gp",   "5 lb.",
        "Estrutura (rígida), Dano (simples), Proteção (leve)" }
      { "Couro, Trapo de",        "1 gp",   "2 lb.",
        "Estrutura (flexível), Proteção (leve)" }
      { "Ervas, Maço de",         "1 gp",   "1 lb.",
        "Estrutura (flexível), Consumível" }
      { "Metal, Minério de",        "5 gp",   "10 lb.",
        "Estrutura (rígida), Dano (avançado), Proteção (heavy)" }
      { "Mineral, Pedra de",       "5 gp",   "5 lb.",
        "Estrutura (detalhada), Dano (avançado)" }
      { "Fluido, Garrafa of",       "5 gp",   "2 lb.",  "Consumível" }
      { "Metal Precioso, Barra de", "5 gp",   "1 lb.",
        "Estrutura (detalhada, condutível), Valioso" }
      { "Gema, Fragmento de",          "10 gp",  "1 lb.",
        "Estrutura (detalhada), Valioso" }
      { "Essência, Fio de",     "10 gp",  "---",
        "Consumível, Valioso" }
    }
  }
  material_value: {
    title: "Conversão de Valores de Materiais"
    headers: { "Operação ou qualidade", "Valor ou resultado" }
    table_class: "w3-twothird",
    column_class: { "", "w3-center" },
    rows: {
      { "Vender", "1.0x" }
      { "Compra", "1.5x" }
      { "Cada nível de raridade maior", "10x" }
      { "Com modificador", "2x" }
      { "Quebrar para raridade menor", "5 unidades" }
    }
  }
  crafting_time: {
    title: "Tempo Adicional por Raridade de Materiais"
    headers: { "Raridade do material", "Semanas adicionais" }
    table_class: "w3-twothird",
    column_class: { "", "w3-center" },
    rows: {
      { "Incomum", "+1" }
      { "Raro", "+2" }
      { "Muito Raro", "+5" }
      { "Lendário", "+10" }
    }
  }
  types_of_potion_of_power: {
    title: "Tipos de Poções do Poder"
    headers: { "Tipo", "Nível máximo do slot", "Preço", "Raridade" }
    column_class: { "", "w3-center", "w3-center", "" },
    rows: {
      { "Poção do Poder",          1, "50 gp", "Comum"      }
      { "Poção do Poder Maior",    2, "---",   "Incomum"    }
      { "Poção do Poder Superior", 3, "---",   "Raro"       }
      { "Poção do Poder Supremo",  5, "---",   "Muito raro" }
    }
  }
  school_per_mod: {
    title: "Escolas de Magia por Modificador"
    headers: { "Escola", "Modificadores associados" }
    column_class: { "", "w3-center" }
    table_class: "w3-twothird"
    rows: {
      { "abjuração", "sagrado" }
      { "conjuração", "retorcido, denso" }
      { "divinação", "energizado, afiado" }
      { "encantamento", "ressoante" }
      { "evocação", "incandescente, gélido" }
      { "ilusão", "místico, fino" }
      { "transmutação", "tóxico, corrosivo" }
      { "necromancia", "profano" }
    }
  }
  simple_melee_weapon_crafting: {
    title: "Armas simples corpo-a-corpo"
    headers: { "Variação", "Valor", "Peso", "Dano", "Propriedades", "Fórmula para manufatura" }
    rows: {
      { "Adaga", "2 gp", "1 lb", "1d4 perfurante",
        "4x, sutil, leve, arremesso 20/60",
        "1x Estrutura (rígda) + 1x Dano (avançado)" }

      { "Azagaia", "5 sp", "2 lb", "1d6 perfurante", "arremesso 30/120",
        "2x Estruturas (rígidas) + 1x Dano (simples)" }

      { "Bordão", "2 sp", "4 lb", "1d6 concussivo", "versátil 1d8",
        "2x Estruturas (rígidas)" }

      { "Foice", "1 gp", "2 lb", "1d4 perfurante", "2x, leve, mortal",
        "1x Estrutura (rígida) + 1x Dano (advanced)" }

      { "Clava", "1 sp", "5 lb", "1d6 concussivo", "",
        "Qualquer objeto pesado o suficiente" }

      { "Clava Grande",  "2 sp", "10 lb", "1d8 concussivo", "duas mãos",
        "Qualquer objeto pesado o suficiente" }

      { "Lança", "1 gp", "3 lb", "1d6 perfurante",
        "infincar, versátil 1d8, arremesso 20/60",
        "3x Estruturas (rígidas) + 1x Dano (simples)" }

      { "Maça", "5 gp", "4 lb", "1d6 concussivo", "",
        "2x Estruturas (rígidas) + 1x Dano (simples)" }

      { "Machadinha",   "5 gp", "2 lb", "1d6 cortante",
        "2x, leve, arremesso 20/60",
        "1x Estrutura (rígda) + 1x Dano (avançado)" }

      { "Martelo leve",  "2 gp", "2 lb", "1d4 concussivo",
        "4x, leve, arremesso 20/60",
        "1x Estrutura (rígda) + 1x Dano (simples)" }

      { "Porrete", "1 sp", "2 lb",  "1d4 concussivo", "leve",
        "Qualquer objeto pesado o suficiente" }
    }
  }
  martial_melee_weapon_crafting: {
    title: "Armas marciais corpo-a-corpo"
    headers: { "Variação", "Valor", "Peso", "Dano", "Propriedades",
               "Fórmula para manufatura" }
    rows: {
      { "Alabarda", "20 gp", "6 lb.", "1d10 slashing",
        "pesada, alcance, duas mãos, fender",
        "2x Estruturas (rígidas) + 2x Dano (avançado)" }

      { "Chicote", "2 gp", "3 lb", "1d4 cortante", "sutil, alcance",
        "3x Estrutura (flexível)" }

      { "Cimitarra", "25 gp", "3 lb", "1d6 cortante", "sutil, leve, mortal",
        "1x Estrutura (rígida) + 2x Dano (avançado)" }

      { "Espada Curta", "10 gp", "2 lb", "1d6 perfurante", "sutil, leve",
        "1x Estrutura (rígida) + 1x Dano (avançado)" }

      { "Espada Grande", "50 gp", "6 lb", "2d6 cortante",
        "pesado, duas mãos, lâmina estável",
        "1x Estrutura (rígida) + 2x Dano (avançado) + 1x Metal Incomum" }

      { "Espada Longa", "15 gp", "3 lb", "1d8 cortante",
        "versátil d10, lâmina estável"
        "1x Estrutura (rígida) + 2x Dano (avançado)" }

      { "Glaive", "20 gp", "6 lb", "1d10 cortante"
        "pesada, alcance, duas mãos, lâmida estável",
        "2x Estruturas (rígidas) + 2x Dano (avançado)" }

      { "Lança de Montaria", "10 gp", "6 lb", "1d12 perfurante",
        "alcance, montaria", "2x Estruturas (rígidas) + 2x Dano (avançado)" }

      { "Lança Longa", "5 gp", "18 lb", "1d10 perfurante",
        "pesada, alcance, duas mãos, infincar",
        "2x Estruturas (rígidas) + 2x Dano (avançado)" }

      { "Maça Estrela", "15 gp", "4 lb", "1d8 perfurante", "",
        "2x Estruturas (rígidas) + 1x Dano (avançado)" }

      { "Machado de Batalha", "10 gp", "4 lb", "1d8 cortante",
        "versátil d10, fender",
        "1x Estrutura (rígida) + 2x Dano (avançado)" }

      { "Machado Grande", "30 gp", "7 lb", "1d12 cortante",
        "pesado, duas mãos, fender",
        "1x Estrutura (rígida) + 3x Dano (avançado)" }

      { "Malho", "10 gp", "10 lb", "2d6 concussivo",
        "pesado, duas mãos, esmurrar",
        "1x Estrutura (rígida) + 2x Dano (avançado) + 1x Dano (qualquer)" }

      { "Mangual", "10 gp", "2 lb", "1d8 concussivo", "",
        "2x Estruturas (rígidas) + 1x Dano (avançado)" }

      { "Martelo de Guerra", "15 gp", "2 lb", "1d8 concussivo",
        "versátil d10, esmurrar",
        "1x Estrutura (rígida) + 2x Dano (avançado)" }

      { "Picareta de Guerra", "5 gp", "2 lb", "1d8 perfurante", "mortal",
        "1x Estrutura (rígida) + 1x Dano (avançado)" }

      { "Rapieira", "25 gp", "2 lb", "1d8 perfurante", "sutil",
        "1x Estrutura (rígida) + 2x Dano (avançado)" }

      { "Tridente", "5 gp", "4 lb", "1d6 perfurante",
        "mortal, versátil 1d8, arremesso 20/60",
        "2x Estruturas (rígidas) + 1x Dano (avançado)" }
    }
  }
}

