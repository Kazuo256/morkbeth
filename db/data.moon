{
  vigor: {
    name: "Vigor",
    tags: { "regra", "revisão" },
    rules: [[
Indica quantas horas uma criatura aguenta viajar por dia. Começa em 12 e se
recupera após um repouso longo. Cada hora de viagem após esgotado o vigor exige
um teste de resistência de Constituição com DC 10 + horas ultrapassadas. Se
falhar, perde um dado de vida e, se não tiver mais dados de vida, ganha um
nível de exaustão.
    ]]
  }
  hunger: {
    name: "Fome",
    tags: { "regra", "revisão" },
    rules: [[
Além do <a
href="https://www.dndbeyond.com/sources/basic-rules/adventuring#FoodandWater">descrito
em PHB 185</a>, valem também as seguintes regras:
<ul>
  <li>Cada dia sem comer causa Fadiga (3)</li>
  <li>Se você comer 2 lb. de comida (e.g. uma ração inteira) em um dia, você
      recupera um dado de vida extra ao final do próximo repouso longo.</li>
</ul>
    ]]
  }
  fresh_food: {
    name: "Comida Fresca",
    tags: { "item", "1 sp, 1 lb.", "novo" },
    rules: [[
Comida obtida através de caça ou coleta. Não dura muito tempo: ela estraga de
um dia para o outro. Um personagem proficiente em *Cook's Utensils* pode,
usando este item durante um repouso longo, conservar comida fresca, produzindo
1 ração de viagem não perecível para cada 4 lb. de alimento fresco usado.
    ]]
  }
  fatigue: {
    name: "Fadiga (X)",
    tags: { "condição", "novo" },
    rules: [[
Quando uma criatura com Fadiga (X) viaja, ela gasta X Vigor a mais por hora.
Múltiplas fontes de Fadiga (X) acumulam-se.
    ]]
  }
  encumbered: {
    name: "Sobrecarga",
    tags: { "condição", "revisão" },
    rules: [[
Uma criatura está com sobrecarga se carregar peso acima de 5 vezes sua Força.
Enquanto estiver sobrecarregada, sua velocidade fica reduzida em 10 ft. Além
disso, ela tem Fadiga (2) se viajar em passo cauteloso ou normal, e Fadiga (12)
se viajar em passo acelerado.
    ]]
  }
  heavily_encumbered: {
    name: "Sobrecarga Pesada",
    tags: { "condição", "revisão" },
    rules: [[
Uma criatura está com sobrecarga pesada se carregar peso acima de 10 vezes sua
Força.  Enquanto estiver com sobrecarga pesada, sua velocidade fica reduzida em
mais 10 ft. e fica com desvantem em testes de habilidade, resistência e ataques
que usem Força, Destreza, ou Constituição. Testes de resistência de
Constituição causados por falta de Vigor falham automaticamente.
    ]]
  }
  auto_travel: {
    name: "Viagem Automática",
    tags: { "regra", "novo" },
    rules: [[
Em certas circunstâncias, por comodidade, os jogadores podem optar por fazer
uma Viagem Automática. Nesse caso, eles sempre viajam em modo Cauteloso e
garantidamente evitam todo e qualquer perigo, a menos do desgaste natural da
viagem. Esse método de viagem só é permitido em duas circunstâncias:
<ul>
  <li>
    Quando os personagens estão voltando de uma expedição pelo mesmo caminho
    que percorreram na ida; ou
  </li>
  <li>
    Quando um personagem se junta a uma expedição após a 1ª sessão da aventura
    (nesse caso, eles seguem sinais deixados pelos jogadores que foram na
    frente).
  </li>
</ul>
    ]]
  }
  tactical_wit: {
    name: "Tactical Wit",
    tags: { "característica de War Mage", "revisão" },
    rules: [[
Essa característica descrita em XGtE 60 passa a adicionar seu bônus de
proficiência para sua iniciativa ao invés de adicionar seu modificador de INT.
    ]]
  }
  wanderer: {
    name: "Wanderer",
    tags: { "característica de Outlander", "revisão" },
    rules: [[
Essa característica descrita em PHB 136 passa a ter o seguinte efeito. Uma vez
por dia, você pode relembrar uma rota que já tenha percorrido em uma região
(ainda requer a atividade de navegação). Adicionalmente, você acha duas vezes
mais comida fresca e água.
    ]]
  }
  bag_of_holding: {
    name: "Bag of Holding",
    tags: { "item mágico incomum", "revisão" },
    rules: [[
Tem sua capacidade reduzida para 250 lb. Um novo item mágico, *greater bag of
holding*, tem a capacidade original da *bag of holding* mas é um item mágico
*raro*.
    ]]
  }
  portable_hole: {
    name: "Portable Hole",
    tags: { "item mágio muito raro", "revisão" },
    rules: [[
Sua raridade aumenta para muito raro.
    ]]
  }
  navigate: {
    name: "Traçar Rota",
    tags: { "atividade de viagem", "revisão" },
    rules: [[
Ao tentar atravessar uma região seguindo uma rota não trivial, um personagem (e
apenas um) *precisa* realizar esta atividade. Isso pode envolver um teste
de habilidade para, por exemplo, achar o melhor caminho através da floresta ou
seguir o rastro de uma criatura. Se falhar, o grupo fica perdido e leva o dobro
do tempo na viagem. Caso contrário, os heróis seguem a rota sem problemas. O
teste só pode ser feito uma vez por dia.

O teste de habilidade depende das características da região e da rota. Ele pode
ser, normalmente, dispensado usando um mapa e/ou uma bússola - embora isso não
dispense a necessidade da atividade em si!
    ]]
  }
  mapping: {
    name: "Mapear Rota",
    tags: { "atividade de viagem", "revisão" },
    rules: [[
Com a ajuda de *cartographer's tools*, um personagem com proficiência nele
pode mapear a rota que estão percorrendo para não precisar fazer um teste de
habilidade durante uma futura atividade de Traçar Rota.
    ]]
  }
  seek: {
    name: "Procurar",
    tags: { "atividade de viagem", "revisão" },
    rules: [[
Se tiver algo que os jogadores queiram encontrar numa região como fontes de
comida, materiais ou simplesmente lugares com determinadas características (por
exemplo, uma clareira), um ou mais deles podem realizar esta atividade.
Dependendo do caso, pode ser preciso ter uma proficiência e/ou fazer um teste
de habilidade.
    ]]
  }
  change_places: {
    name: "Mudar de Local",
    tags: { "atividade de exploração", "novo" },
    rules: [[
Andar entre salas, seguir por um túnel, atravessar um trecho da montanha - são
todos exemplos de Mudar de Local. O tempo gasto depende da escala do local
explorado, mas sempre supõe-se que os jogadores estão sendo cautelosos e
atentos e, portanto, avançam relativamente devagar a menos que digam o
contrário. Assim, esta ação normalmente dura 10 minutos em ambientes fechados e
1 hora em regiões abertas, adicionando um dado à Pilha de Tensão se for o caso.
    ]]
  }
  comb: {
    name: "Vasculhar",
    tags: { "atividade de exploração", "novo" },
    rules: [[
Inclui ações como procurar armadilhas, investigar um móvel, detectar passagens
secretas, identificar marcas na parede, etc. O tempo depende da escala do
local: 10 minutos para ambientes fechados e 1 hora para ambientes abertos. Essa
ação anula sua percepção passiva pode tanto adicionar um dado à Pilha de Tensão
quanto rolar a Pilha, dependendo da situação.
    ]]
  }
  harvest: {
    name: "Coletar",
    tags: { "atividade de exploração", "novo" },
    rules: [[
Quando um material não é encontrado pronto para levar, pode ser preciso passar
um tempo retirando-o de onde está para poder carregá-lo. Tipicamente isso
envolve retirar materiais de corpos de monstros e caçar alimento na natureza.
Pode ser necessário um teste de habilidade mas, independente disso, essa ação
sempre leva uma hora. Essa ação anula sua percepção passiva e pode
exigir rolar a Pilha de Tensão, além de adicionar dados de acordo com a escala
do ambiente.
    ]]
  }
  extended_rest: {
    name: "Repouso Estendido",
    tags: { "regra", "novo" },
    rules: [[
No lugar de um repouso longo, é possível fazer um *repouso estendido* com
8 horas de repouso (6 de sono e 2 de atividades leve, como de costume). Os
benefícios são os mesmos de um repouso curto e a criatura recupera todo seu
Vigor. Além disso, um membro do grupo pode Fortalecer seus Laços e, se uma
criatura comeu pelo menos o dobro do que precisa diariamente, ela recupera um
dado de vida.
    ]]
  }
  long_rest: {
    name: "Repouso Longo",
    tags: { "regra", "revisão" },
    rules: [[
Só é possível fazer um repouso longo em um lugar seguro como Sharpridge e esse
repouso leva uma semana inteira (5 dias, no caso). Tirando a primeira semana
após o retorno de uma expedição, os jogadores precisam pagar pelos custos de
vida apropriados ou acumular Exaustão (ver regras de *downtime*).
    ]]
  }
  bonding: {
    name: "Fortalecer Laços",
    tags: { "regra", "novo" },
    rules: [[
Você tenta animar ou de alguma forma da apoio às pessoas do grupo. Descreva
como você usa uma proficiência, item, magia, característica (da sua classe,
raça, ou *background*) ou talento para ajudar uma ou mais pessoas. Pode
ser preciso fazer um teste de habilidade.

Se a ação for bem sucedida, todos no grupo recuperam uma quantidade de dados de
vida igual aos seus respectivos bônus de proficiência. Alternativamente, um
personagem pode recuperar *slots* de magia cujo nível total seja no máximo
metade do bônus de proficiência, arredondado para cima.

Independente da ação dar certo ou não, você forma um Laço com as pessoas que
estavam ali com você. A qualquer momento que você precisar fazer um teste de
habilidade, jogada de ataque ou teste de resistência, e um jogador que esteja a
5 ft. de você que faça parte do seu Laço pode gastar o Laço para te dar vantagem
naquela jogada. Você não pode Fortalecer Laços novomente enquanto tiver um Laço
guardado.
]]
  }
  charged_blow: {
    name: "Ataque Calculado",
    tags: { "ataque com arma", "novo" },
    rules: [[
Durante seu turno, quando você faz um ataque com arma, você pode gastar sua
reação para atrasar este ataque sobre um alvo. Ao final do próximo turno desse
alvo, o ataque é completado. Se o alvo sair do seu alcance até então, o
Ataque Calculado falha automaticamente. Caso contrário, se acertar, o golpe é
garantidamente crítico.
]]
  }
  disarm: {
    name: "Desarmar",
    tags: { "ataque com arma", "revisão" },
    rules: [[
Você pode usar um ataque com arma para derrubar a arma ou algum item que um
alvo estiver segurando nas mãos. Você faz um jogada de ataque contestada por um
teste de Força (Atletismo) ou Destreza (Acrobacia) do alvo. Se você ganhar. seu
ataque não causa dano nem efeitos danosos mas, ao invés disso, o alvo solta o
item no chão.

Você tem desvantagem se o alvo estiver segurando o item com duas ou mais mãos.
O alvo tem vantagem se for de um tamanho maior que o seu e desvantagem se for
menor.
]]
  }
  distract: {
    name: "Distrair",
    tags: { "ação", "novo" },
    rules: [[
Você fala e age de maneira a distrair uma criatura que consiga claramente lhe
ver e ouvir para atrair sua atenção. Você contesta um teste de Carisma
(Performance) contra um teste de Sabedoria (Intuição) do alvo. Se você
ganhar, eles ficam com desvantagem em testes de Sabedoria (Percepção) para
detectar qualquer coisa que não seja você até o começo do seu próximo turno.
]]
  }
  intercept: {
    name: "Interceptar",
    tags: { "reação", "novo" },
    rules: [[
Se você estiver empunhando uma arma corpo-a-corpo ou um escudo, sempre que uma
criatura errar um ataque corpo-a-corpo contra você, você pode escolher usar sua
reação para se aproveitar da falha deles mas com o risco de não conseguir mais
bloquear o golpe. Faça um teste de Força contestado por um teste de Força da
criatura atacante.  Se você perder, o ataque não só passa a te acertar como é
considerado crítico. Se ganhar, o atacante fica Atordoado até o final do
próximo turno dele.
]]
  }
  shove: {
    name: "Empurrar",
    tags: { "ataque corpo-a-corpo", "revisão" },
    rules: [[
Quando você sucede em Empurrar uma criatura no lugar de um ataque, além de
mover para longe ou derrubar o adversário, você pode escolher, ao invés disso,
deixá-lo Desequilibrado até o final do próximo turno dele ou até receber dano,
o que acontecer primeiro.
]]
  }
  frightened: {
    name: "Amedontrado",
    tags: { "condição", "revisão" },
    rules: [[
Além dos efeitos normais, você não pode usar magias com componente verbal.
]]
  }
  prone: {
    name: "Caído",
    tags: { "condição", "revisão" },
    rules: [[
Além dos efeitos normais, levantar da condição Caído causa ataques de
oportunidade a não ser que você use a ação Desengajar antes. Ataques de
oportunidade desencadeados assim não se beneficiam de vantagem causada pela
condição de Caído.
]]
  }
  restrained: {
    name: "Impedido",
    tags: { "condição", "revisão" },
    rules: [[
Além dos efeitos normais, você não pode usar magias com componente somático.
]]
  }
  stunned: {
    name: "Atordoado",
    tags: { "condição", "revisão" },
    rules: [[
Além de falhar automaticamente em testes de resistência de Força e Destreza,
você também falha automaticamente qualquer teste de habilidade usando Força ou
Destreza.
]]
  }
  unsteady: {
    name: "Desequilibrado",
    tags: { "condição", "novo" },
    rules: [[
<ul>
  <li>Ataques contra você têm vantagem</li>
  <li>Você tem desvantagem em testes de resistência de Força ou Destreza</li>
</ul>
]]
  }
  sluggish: {
    name: "Molenga",
    tags: { "condição", "novo" },
    rules: [[
<ul>
  <li>Você não pode reagir nem interagir com objetos como parte de ações ou
  movimento</li>
  <li>Você não pode fazer uma ação E uma ação bônus no mesmo turno</li>
  <li>Você não pode fazer mais de um ataque por ação, independente de
  habilidades e itens</li>
</ul>
]]
  }
  slowed: {
    name: "Lento",
    tags: { "condição", "novo" },
    rules: [[
<ul>
  <li>Você fica Molenga</li>
  <li>Você tem -2 em AC e testes de resistência de Destreza</li>
  <li>Com 50% de chance, lançar magias de uma ação exige repetir a ação no
  próximo turno para completar o efeito</li>
</ul>
]]
  }
  deep_wound: {
    name: "Feridas Produndas",
    tags: { "regra", "novo" },
    rules: [[
Quando você fica com 0 pontos de vida, suas feridas são profundas e lhe causam
um nível de Exaustão. Você pode temporariamente reduzir a Exaustão gerada por
essa regra com ajuda de um *healer's kit* durante um repouso curto. Um uso
do *healer's kit* reduz um nível de Exaustão por 4 horas. Você pode incluir
mais usos de uma vez só para reduzir mais níveis OU estender o efeito por mais
4 horas (só um desses efeito por uso adicional).

Alternativamente, alguém pode tentar um teste de Sabedoria (Medicina) com DC 10
para reduzir um nível de Exaustão por um número de horas igual ao quanto o
teste rolou acima de 10. Só pode haver uma redução temporária de Exaustão por
vez.
]]
  }
  death_mark: {
    name: "Marca da Morte",
    tags: { "regra", "novo" },
    rules: [[
Além das feridas profundas, quando você cai para 0 pontos de vida, você deve
fazer um teste de Constituição com DC 10. Se você falhar, você ganha uma
Marca da Morte. Assim que você acumular 3 Marcas da Morte, você irá morrer nas
próximas 24 horas a não ser que alguma das marcas seja removida de alguma
maneira. Isso significa que, em média, um personagem aguenta cair para 0 pontos
de vida umas 7 vezes.

Marcas da Morte são difíceis de remover. Apenas efeitos mágicos capazes de
restaurar partes do corpo podem removê-las e eles só podem remover uma marca
por vez.
]]
  }
  focused_strike: {
    name: "Golpe Focado",
    tags: { "ataque com arma", "novo" },
    rules: [[
Algumas propriedades de armas permitem que você faça um ataque chamado Golpe
Focado. Isso significa que ela causa um efeito especial contanto que você
gaste uma quantidade de movimento igual à metade da sua velocidade total (da
mesma maneira que ocorre quando você se levanta da condição de Caído). Esse
ataque substitui um (e apenas um) dos ataques que você pode fazer no turno e só
pode ser feito usando uma arma. Você fica Desequilibrado até o final do seu
próximo turno ou até receber dano, o que acontecer primeiro.
]]
  }
  loading: {
    name: "Recarga",
    tags: { "propriedade de arma", "revisão" },
    rules: [[
Além dos efeitos em PHB 147, seus ataques com armas de Recarga têm vantagem se
você estiver Caído.
]]
  }
  finesse: {
    name: "Sutil",
    tags: { "propriedade de arma", "revisão" },
    rules: [[
Ao atacar com armas Sutis, sua jogada de ataque e de dano SÓ podem
usar seu modificador de Destreza.
]]
  }
  pound: {
    name: "Esmurrar",
    tags: { "propriedade de arma", "novo" },
    rules: [[
Se você acertar um Golpe Focado com esta arma, além do dano normal, você também
empurra para longe o alvo um número de pés igual ao dano (arredondado para o
próximo múltiplo de 5 em uma grade). O alvo fica Caído.
]]
  }
  cleave: {
    name: "Fender",
    tags: { "propriedade de arma", "novo" },
    rules: [[
Ao fazer um Golpe Focado com esta arma, seu alvo e qualquer outro oponente
adjacente a ele que esteja ao seu alcance realiza um teste de resistência de
Destreza contra jogada de ataque. Todos que falharem são atingidos pelo ataque.
]]
  }
  pin: {
    name: "Infincar",
    tags: { "propriedade de arma", "novo" },
    rules: [[
Ao acertar um Golpe Focado com esta arma, além de causar dano normalmente,
o alvo fica Agarrado pela sua arma infincada na ferida. Até você soltar o
alvo com uma ação livre ou o alvo se libertar, você só pode usar esta arma
para atacar a criatura presa - o que você faz com vantagem e sem soltá-la.
]]
  }
  bladelock: {
    name: "Lâmina Estável",
    tags: { "propriedade de arma", "novo" },
    rules: [[
Uma vez durante seu turno, quando você fizer um ataque com esta arma, você pode
gastar todo movimento restante para assumir uma postura defensiva. Até o começo
do seu próximo turno ou até você receber um ataque, o que acontecer primeiro,
você tem +2 AC.
]]
  }
  deadly: {
    name: "Mortal",
    tags: { "propriedade de arma", "novo" },
    rules: [[
Ataques com vantagem usando esta arma rolam um dado adicional de dano da arma.
]]
  }
  sturdiness_die: {
    name: "Dado de Robustez",
    tags: { "regra", "novo" },
    rules: [[
Todo escudo tem um Dado de Robustez que indica quantas vezes você consegue
Bloquear com ele antes de quebrar e se tornar inútil. Sempre que um efeito
fizer você rolar o Dado de Robustez do seu escudo, se sair 1, ele quebra. Isso
significa que, em média, um escudo pode Bloquear uma quantidade de vezes igual
a metade do tamanho do seu Dado de Robustez.

Uma vez quebrado, é possível consertar um escudo gastando um material
*Structure (hard)* da mesma raridade que o escudo e seguindo as regras em XGtE
84-85.
]]
  }
  shield_block: {
    name: "Bloquear",
    tags: { "reação", "novo" },
    rules: [[
Sempre que uma personagem usando um escudo receber dano ao qual não é
vulnerável, causado por um ataque ou um teste de resistência de Destreza, ela
pode usar sua reação para Bloquear. Isso a torna resistente a essa instância de
dano específica, ou imune se já for resistente a ele. Depois disso, ela rola o
Dado de Robustez atual do escudo.
]]
  }
  small_shield: {
    name: "Pequeno",
    tags: { "propriedade de escudo", "novo" },
    rules: [[
Se um escudo for Pequeno, ele só acresce +1 na sua AC mas a mão em que ele está
conta como uma mão livre, exceto para realizar ataques com armas que não sejam
o próprio escudo.
]]
  }
  large_shield: {
    name: "Grande",
    tags: { "propriedade de escudo", "novo" },
    rules: [[
Com este escudo, você pode, no seu turno. gastar metade da sua velocidade em
movimento para se proteger atrás dele como se fosse ¾ de cobertura. Ou seja,
até o começo do seu próximo turno, ele confere +5 AC ao invés de +2. Esse
bônus não se soma com o efeito de outras coberturas.
]]
  }
  rest_activity: {
    name: "Repousar",
    tags: { "atividade de tempo livre", "revisão" },
    rules: [[
Você usa a semana para realizar um Repouso Longo. Por padrão, supõe-se que essa
é a atividade que você realiza durante a primeira semana após uma aventura caso
nenhuma outra atividade seja especificada.
]]
  }
  subsist: {
    name: "Sustentar-se",
    tags: { "atividade de tempo livre", "revisão" },
    rules: [[
Você trabalha, contribuindo para a comunidade das cidades aliadas em troca de
ganhos o suficientes para manter uma condição de vida modesta. Por padrão,
supõe-se que personagens fazem isso depois da primeira semana após uma
aventura caso nenhuma outra atividade seja especificada.
]]
  }
  crafting: {
    name: "Manufaturar Item",
    tags: { "atividade de tempo livre", "revisão" },
    rules: [[
Com as proficiências e ferramentas adequadas, você pode usar materiais para
manufaturar seus próprios itens. Veja as regras sobre manufatura para mais
detalhes. Podem ser necessárias múltiplas semanas dedicadas a essa atividade
para concluir um item de raridade maior. No mínimo, leva uma semana.
]]
  }
  animal_care: {
    name: "Cuidar das Montarias",
    tags: { "atividade de tempo livre", "novo" },
    rules: [[
Você pode pagar 10 gp por montaria em cuidados dedicados. Após uma semana, as
montarias cuidadas têm efeito de Fadiga causada por sobrecarga reduzido pela
metade. Esse bônus só vale por uma aventura.
]]
  }
  sacred_ritual: {
    name: "Ritos Sagrados",
    tags: { "atividade de tempo livre", "novo" },
    rules: [[
Se sua classe e/ou arquétipo está atrelado a uma divindade, você pode gastar 10
gp em oferendas e uma semana em rituais para ganhar uma bênção do seu deus.
Essa bênção é sempre um efeito de uso único durante sua próxima aventura que
confere vantagem a um teste de habilidade.

A benção que você recebe segue sua divindade de acordo com a tabela *Benção
por Divindade*. Só é possível ter uma bênção por vez mas, para cada vez que
você triplicar o valor oferecido, você terá um uso a mais do efeito.

<h4>Benção por Divindade</h4>

<table class="w3-table-all w3-card-4">
  <tr class="w3-dark-gray">
    <th>Deus</th>
    <th>Teste que pode abençoar</th>
  </tr>
  <tr>
    <td>A Chama</td><td>Percepção, Intuição</td>
  </tr><tr>
    <td>A Terra</td><td>Natureza, Sobrevivência, Adestrar Animais</td>
  </tr><tr>
    <td>O Olho</td><td>História, Investigação</td>
  </tr><tr>
    <td>O Coração</td><td>Medicina, Religião</td>
  </tr><tr>
    <td>O Osso</td><td>Enganação, Furtividade</td>
  </tr><tr>
    <td>O Martelo</td><td>Atletismo, Acrobacia</td>
  </tr><tr>
    <td>A Tempestade</td><td>Arcana, Intimidação</td>
  </tr><tr>
    <td>A Voz</td><td>Performance, Truque de Mãos</st>
  </tr>
</table>
]]
  }
  magic_studies: {
    name: "Estudar Magia",
    tags: { "atividade de tempo livre", "novo" },
    rules: [[
Você pode pagar 50 gp (+50 gp por nível da magia) e passar uma semana
pesquisando novas técnicas e descobrindo novos segredos mágicos. Você pode
substituir um cantrip ou magia conhecida por outra de mesmo nível e da mesma
lista. Não se aplica a magias preparadas.
]]
  }
  preventive_research: {
    name: "Pesquisar",
    tags: { "atividade de tempo livre", "novo" },
    rules: [[

<div class="w3-container w3-half">

  <span style="display: block; height: 2em"></span>
  Com a ajuda dos especialistas das Cidades Aliadas, você pode pagar uma
  quantia e passar uma semana pesquisando um local, uma espécie, um fato
  histórico, uma lenda, um item, um indivídio ou outro aspecto isolado
  relacionado à aventura para a qual estão se preparando. O valor depende do
  nível da região ou indivíduo segundo a tabela *Custo de Pesquisa por
  Nível* e, caso não seja possível pesquisar o assunto escolhido, o mestre
  avisará antes.
  <span style="display: block; height: 2em"></span>
</div>

<div class="w3-container w3-half">
  <h4>Custo de Pesquisa por Nível</h4>
  
  <table class="w3-table-all w3-card-4">
    <tr class="w3-dark-gray">
      <th>Níveis</th>
      <th>Valor</th>
    </tr>
    <tr>
      <td>1-2</td><td>50gp</td>
    </tr><tr>
      <td>3-5</td><td>100gp</td>
    </tr><tr>
      <td>6-8</td><td>500gp</td>
    </tr><tr>
      <td>9-11</td><td>1000gp</td>
    </tr><tr>
      <td>12-14</td><td>5000gp</td>
    </tr><tr>
      <td>15-17</td><td>10000gp</td>
    </tr><tr>
      <td>18-20</td><td>25000gp</td>
    </tr>
  </table>

  <span style="display: block; height: 2em"></span>
</div>
]]
  }
  gear_and_tools: {
    name: "Mantimetos e Ferramentas aprimoradas",
    tags: { "expansão" },
    rules: [[
Conforme mais pessoas chegam à Sharpridge, elas trazem consigo mais itens ou
conhecimento sobre manufatura de itens. Isso permite a venda e manufatura de
mantimentos e ferramentas cada vez mais valiosos, segundo a tabela
*Mantimentos e Ferramentas Expandidos*. Para obter um aprimoramento
desses, é preciso ter todos os que o precedem.

<div class="w3-container w3-quarter"></div>
<div class="w3-container w3-half">
  <h4>Mantimentos e Ferramentas Expandidos</h4>
  table:gear_expansions
</div>
<div class="w3-container w3-quarter"></div>

<span style="display: block; height: 20em"></span>
]]
  }
  craftsmen: {
    name: "Artesãos",
    tags: { "expansão" },
    rules: [[
Para equipamentos e items mágicos, apenas artesões habilidosos o suficiente
conseguem trabalhar com materiais mais raros.

A tabela *Artesãos Expandidos* indica quantos pontos de expansão são
precisos para garantir um artesão capaz de lidar com materiais da raridade
indicada.  Inclusive, o nível "Comum" permite a venda e manufatura de armas
marciais e armaduras pesadas. Cada nível acima de "Comum" requer todos os que o
precedem já tenham sido liberados.

<div class="w3-container w3-quarter"></div>
<div class="w3-container w3-half">
  <h4>Artesãos Expandidos</h4>
  table:crafting_expansions
</div>
<div class="w3-container w3-quarter"></div>

<span style="display: block; height: 16em"></span>
]]
  }
  library: {
    name: "Biblioteca",
    tags: { "expansão" },
    rules: [[
Especialistas dos mais variados tipos chegam à Sharpridge vindo dos Reinos
Livres. Uma biblbioteca concentra todo o conhecimento dos exilados em um único
lugar para fácil acesso... por um preço, é claro.

Um jogador pode pagar aos estudiosos de Sharpridge para descobrir onde é
possível encontrar um material específico ou, pelo dobro do valor, materiais
com determinadas características gerais (por exemplo, materiais "raros e
incandescentes"). Cada expansão da biblioteca permite pesquisar materiais de
raridades maiores, conforme a tabela *Biblioteca Expandida* — que inclui
também os preços.

<div class="w3-container w3-quarter"></div>
<div class="w3-container w3-half">
  <h4>Biblioteca Expandida</h4>
  table:library_expansions
</div>
<div class="w3-container w3-quarter"></div>

<span style="display: block; height: 18em"></span>
]]
  }
  scavengers: {
    name: "Catadores",
    tags: { "expansão" },
    rules: [[
Conforme as pessoas de Sharpridge ganham treinamento e aprendem com o exemplo
dos heróis, elas ficam mais dispostas à se arriscar Morkbeth adentro para
trazer riquezas a Sharpridge. Cada expansão aumenta o nível das regiões nas
quais os catadores conseguem buscar materiais conforme a tabela *Catadores
Expandidos*. Os jogadores podem então encomendá-los por 150% do valor na
cidade e cada entrega leva 1 semana. A região precisa ser conhecida e ter uma
rota mapeada para ela, caso necessário.

<div class="w3-container w3-quarter"></div>
<div class="w3-container w3-half">
  <h4>Catadores Expandidos</h4>
  table:scavenging_expansions
</div>
<div class="w3-container w3-quarter"></div>

<span style="display: block; height: 18em"></span>
]]
  }
  outposts_and_roads: {
    name: "Postos Avançados e Estradas",
    tags: { "expansão" },
    rules: [[
Com a quantidade de exilados chegando a Sharpridge, é importante expandir e
fazer postos avançados em Morkbeth com estradas providenciando rotas seguras
para navegar. A tabela *Postos Avançados e Rotas* mostra o custo em pontos
de expansão, dinheiro e tempo para fazer um posto avançado ou estrada nova.

Um posto avançado só pode ser construído em um lugar minimamente seguro a no
máximo 1 dia de viagem de uma cidade aliada ou outro posto. Pode ser preciso
mapear a rota antes. Postos avançados incluem uma rota ligando a eles no preço.

<div class="w3-container w3-quarter"></div>
<div class="w3-container w3-half">
  <h4>Postos Avançados e Rotas</h4>
  table:territory_expansions
</div>
<div class="w3-container w3-quarter"></div>

<span style="display: block; height: 10em"></span>
]]
  }
  structure: {
    name: "Estrutura"
    tags: { "tipo de material" }
    rules: [[
Propriedade de materiais usados em itens não consumíveis. Tem os parâmetros:

**Rígido, flexível ou detalhado**. Materiais de estrutura
só podem ter uma dessas atribuições. Elas servem para limitar as fórmulas em
que o material é utilizado. Estruturas duras, como madeira e metais, podem ser
usados em hastes de armas, por exemplo. Estruturas macias servem mais para
armaduras leves.  Estruturas detalhadas são usadas em equipamentos menores e,
em geral, valiosos como anéis e amuletos.

**Inflamável**. Equipamentos feitos com estruturas inflamáveis
entram em combustão se estiverem em contato com fogo ao final de um turno,
sendo perdidas após uma rodada nesse estado. Requer uma ação para apagar.
Criaturas que encostam ou têm posse do equipamento no começo do seu turno
levam 1d6 de dano de fogo.

**Condutível**. Equipamentos feitos com estruturas altamente
condutoras fazem seu portador ser vulnerável a dano elétrico (a não ser que já
seja imune). O portador também tem desvantagem em testes de resistência de
Destreza contra efeitos que causariam dano elétrico.

***Raridades maiores.*** Para cada nível de raridade acima do exigido em uma
receita, materiais com a propriedade $structure acrescentam os seguintes
efeitos no item resultante cumulativamente:

+ *Armas* - +1 em jogadas de ataque
+ *Escudos* - aumenta o tamanho do dado de robustez
+ *Armaduras* - reduz uma penalidade (pelo menos 2 raridades acima)

]]
  }
  harm: {
    name: "Dano"
    tags: { "tipo de material" }
    rules: [[
Propriedade de materiais usados em armas e munições. Possuem um parâmetro:

**Simples ou avançado.** Alguns tipos de materiais são mais adequados
que outros para a manufatura de formas danosas.

***Raridades maiores.*** Para cada nível de raridade acima do exigido em uma
receita, materiais com a propriedade $harm aumentam o dano da arma resultante
em 1
]]
  }
  protection: {
    name: "Proteção"
    tags: { "tipo de material" }
    rules: [[
Propriedade de materiais usados em armaduras e escudos. Possuem um parâmetro:

**Leve ou pesado.** Alguns tipos de materiais provêm defesas mais densas que
outros.

***Raridades maiores.*** Para cada nível de raridade acima do exigido em uma
receita, materiais com a propriedade $protection aumentam a AC de
armaduras e escudos em 1. Para *plate armor*, se todos os materiais de
$protection forem lendários, ela ganha AC +3.
]]
  }
  consumable: {
    name: "Consumível"
    tags: { "tipo de material" }
    rules: [[
Propriedade de materiais usados em poções e outros itens consumíveis. Em geral
possuem modificadores que determinam o efeito do item produzido.

***Raridades maiores.*** Normalmente aumenta o nível do item, como de uma
*potion of healing* para uma *potion of greater healing*.
]]
  }
  valuable: {
    name: "Valioso"
    tags: { "tipo de material" }
    rules: [[
Propriedade de materiais valiosos usados em componentes materiais para magias
e, similarmente, itens mágicos (não consumíveis). Em geral possuem
modificadores que determinam o efeito do item produzido.

***Raridades maiores.*** Em itens mágicos que usam magias, materiais mais
raros que o exigido na fórmula permitem o uso de magias de níveis maiores. Se
a manufatura do item exige que alguém lance a magia no processo, então ela
também precisa ser lançada em um nível maior correspondente.
]]
  }
  fine_mod: {
    name: "Fino"
    tags: { "modificador de material" }
    rules: [[
Ganhe um dado de especialidade em testes de habilidade para causar uma
impressão forte que envolvam empunhar, vestir ou de alguma forma expor
explicitamente o item resultante.
]]
  }
  dense_mod: {
    name: "Denso"
    tags: { "modificador de material" }
    rules: [[
Ao manufaturar itens não-consumíveis, este material vale como duas unidades do
mesmo material, só que sem o modificador `denso`.
]]
  }
  sharp_mod: {
    name: "Afiado"
    tags: { "modificador de material" }
    rules: [[
Testes de habilidade usando o item resultante ganham um dado de especialidade.
]]
  }
  incandescent_mod: {
    name: "Incandescente"
    tags: { "modificador de material" }
    rules: [[
O item resultante emite uma luz pequena (+1) contanto que esteja sendo vestido
ou que você segure ele em uma mão. Você só pode se beneficiar desse efeito uma
vez não importa quantos itens incandescentes tenha.
]]
  }
  freezing_mod: {
    name: "Gélido"
    tags: { "modificador de material" }
    rules: [[
Alimento fresco em contato com o item resultante demora o dobro do tempo para
estragar.
]]
  }
  corrosive_mod: {
    name: "Corrosivo"
    tags: { "modificador de material" }
    rules: [[
Quando você acertar um ataque contra um objeto inanimado e mundano e o item
resultante entrar em contato com esse objeto como parte do ataque, o ataque é
sempre crítico.
]]
  }
  charged_mod: {
    name: "Energizado"
    tags: { "modificador de material" }
    rules: [[
Enquanto você vestir ou segurar em uma mão o item resultante, ele ajuda a
determinar o Norte do Plano Material, possivelmente acrescentando um dado de
especialidade em testes de navegação.
]]
  }
  toxic_mod: {
    name: "Tóxico"
    tags: { "modificador de material" }
    rules: [[
Ao manufaturar itens consumíveis, este material vale como duas unidades do
mesmo material, só que sem o modificador `tóxico`.
]]
  }
  profane_mod: {
    name: "Profano"
    tags: { "modificador de material" }
    rules: [[
Testes para esconder o item resultante ou para uma criatura vestindo o item se
esconder ganham um dado de especialidade contanto que esteja em `luz fraca`.
]]
  }
  holy_mod: {
    name: "Sagrado"
    tags: { "modificador de material" }
    rules: [[
Você pode usar uma ação para empunhar o item resultante em uma mão e rezar
brevemente para ganhar um dado de especialidade no próximo teste de resistência
que fizer até o final do seu próximo turno.
]]
  }
  warped_mod: {
    name: "Retorcido"
    tags: { "modificador de material" }
    rules: [[
O item resultante possui metade do peso usual (efeito não cumulativo).
]]
  }
  resonant_mod: {
    name: "Ressoante"
    tags: { "modificador de material" }
    rules: [[
Enquanto você for a última criatura a passar 24 horas completas em posse do
item resultante, ele emite um som sutil que só você consegue ouvir em um raio
de 60 pés.
]]
  }
  occult_mod: {
    name: "Místico"
    tags: { "modificador de material" }
    rules: [[
Como uma ação bônus, você pode mudar a aparência deste objeto para uma que seja
compatível com sua forma original (esse efeito é uma ilusão).
]]
  }
  potion_of_power: {
    name: "Poção do Poder"
    tags: { "raridade varia", "item" }
    rules: [[
Quando você bebe essa poção, você perde um dado de vida e recupera 1 slot de
magia usado de sua escolha – quanto melhor a qualidade da poção, maior o nível
máximo do slot. Se você não tiver nenhum dado de vida, você ganha um nível de
exaustão ao invés disso.

table:types_of_potion_of_power
]]
  }
  crystal_of_return: {
    name: "Cristal do Retorno"
    tags: { "raro", "item" }
    rules: [[
Você pode segurar esse cristal com uma mão livre e usar uma ação para lançar
uma versão reduzida de *Teleportation Circle*, que é instântanea e só pode
teleportar você e seus aliados para um círculo fixo ou portátil
pré-determinado. Para estabelecer a ligação com tal círculo, é preciso tocá-lo
com o cristal. Se você tocar outro círculo enquanto o cristal já possuir uma
ligação prévia, a ligação nova sobreescreve a antiga. Além disso, ao usar o
cristal, todas as criaturas teletransportadas ganham um nível de exaustão.
]]
  }
  cipher: {
    name: "Cifra"
    tags: { "sintonia", "raridade varia", "item" }
    rules: [[
Pequenos objetos mágicos de diversos formatos e aparências. Durante sua
criação, uma magia é cifrada dentro deles. Uma criatura sintonizada com a cifra
pode ativar a magia segurando o objeto em uma mão livre da mesma maneira que um
pergaminho mágico, mas não precisa ter habilidade de lançar magias ou ter a
magia na sua lista de magias.

Sempre que ativar uma cifra, role um d6. Se sair 1, a cifra é destruída
permanentemente.

A raridade da cifra depende do nível da magia da mesma forma que pergaminhos
mágicos também.  Os materiais necessários para fazer uma cifra dependem da
escola de magia associada, segundo a tabela *Escolas de magia por modificador*.
Além disso, é preciso que uma criatura lance a magia em questão como parte do
processo.

<div class="w3-row" style="margin-bottom: 1em">
<div class="w3-container w3-col" style="width: 16.67%"></div>
table:school_per_mod
<div class="w3-container w3-col" style="width: 16.67%"></div>
</div>

]]
  }
  talisman_of_burden: {
    name: "Talismã do Fardo"
    tags: { "sintonia", "raro", "item" }
    rules: [[
Enquanto você estiver sintonizado com esse item e houver pelo menos uma
criatura aliada a 30 pés de você, você tem resistência a todo dano. No entanto,
sempre que você tomar dano nessas condições, a criatura aliada mais próxima de
você toma o mesmo dano. Você não pode dessintonizar desse item normalmente.
]]
  }
  kingsblade: {
    name: "Dever da Coroa"
    tags: { "sintonia", "raro", "arma (espaga larga)" }
    rules: [[
*Requer sintonia com um paladino do juramento da coroa*

Você ganha um bônus de +1 em jogadas de ataques e de dano usando esta arma.

Sempre que você receber dano que teria acertado uma criatura aliada, a espada
ganha uma carga, até uma quantidade total de cargas igual ao seu bônus de
proficiência.

Como uma ação, você pode gastar uma ou mais cargas para enfincar a espada em
uma superfície e projetar uma linha reta de luz divina de 5 pés de largura, 5
pés de altura e 30 pés de comprimento ao longo dessa superfície (e quaisquer
eventuais curvas e quinas).

A luz dura por 1 minuto ou enquanto você mantiver sua concentração. Cada
criatura inimiga dentro da área da luz quando você a cria faz um teste de
resistência de Constituição, recebendo 2d6 de dano radiante por carga usada e
ficando cegas até o fim do próximo turno delas numa falha ou só recebendo
metade do dano em caso de sucesso. Mortos-vivos têm desvantagem no teste. Esse
mesmo efeito ocorre quando uma criatura inimiga entra na área pela primeira vez
num turno ou termina seu turno nela. Criatura aliadas na área além de você
recuperam 1d6 pontos de vida por carga usada uma única vez.
]]
  }
  weapon_of_shadowbleeding: {
    name: "Arma Sangra-Sombra"
    tags: { "sintonia", "incomum", "arma (sutil)" }
    rules: [[
Essa arma mágica é capaz de cortar pequenas fendas para Shadowfell. Ela tem uma
quantidade de cargas igual ao seu bonus de proficiência, que se recuperam todo
amanhecer.

Sempre que você atacar com ela e acertar, pode escolher gastar uma carga para
ativar seu efeito. A criatura atingida fica impregnada com escuridão mágica
como a causada pela magia *trevas*. Essa escuridão afeta apenas a criatura
atingida e se move junto com ela. A escuridão se dissipa após 1 minuto.

Alternativamente, você pode usar uma ação para causar o efeito em um espaço de
5 por 5 pés dentro do alcance da arma. Nesse caso, a escuridão cobre todo o
espaço atingido, não se move, e afeta todas as criaturas, inclusive você.
]]
  }
}
